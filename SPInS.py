#!/usr/bin/env python
# coding: utf-8
# $Id: SPInS.py
# Author: Daniel R. Reese <daniel.reese@obspm.fr>
# Copyright (C) Daniel R. Reese and Yveline Lebreton
# Copyright license: GNU GPL v3.0
#
#   SPInS is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with SPInS.  If not, see <http://www.gnu.org/licenses/>.
#

"""
A module which contains the main program for SPInS as well as various classes
which intervene when calculating the priors and likelihood function:

- :py:class:`Distribution`: a class which represents a probability distribution
- :py:class:`Prior_list`: a class with a list of priors
- :py:class:`Likelihood`: a class used to represent the likelihood function
- :py:class:`Probability`: a class which groups the priors and likelihood function together

This module relies on the `emcee <http://dan.iel.fm/emcee/current/>`_ package to apply
an MCMC algorithm which will return a representative sample of models for a given set
of seismic an classic constraints.

.. warning::
  In various places in this module, for instance in the :py:class:`Prior_list`
  and :py:class:`Likelihood` classes, various methods return what is described
  as a :math:`\chi^2` value.  Technically, these are not :math:`\chi^2` values,
  but rather :math:`-\chi^2/2`, i.e. the argument of the exponential function
  which intervenes in the Gaussian probability distribution.
"""

__docformat__ = 'restructuredtext'
__version__ = "1.1.0"

# SPInS configuration option
import SPInS_configure as config # user-defined configuration parameters

import dill
import os
import sys
import time
import shutil
import math
import matplotlib
if (config.backend is not None): matplotlib.use(config.backend)
import matplotlib.pyplot  as plt
import matplotlib.patches as mpatches
import matplotlib.lines   as mlines
import numpy as np
from scipy.stats import truncnorm
import emcee
import corner
if (config.PT): import ptemcee
from multiprocessing import Pool, set_start_method

# import modules from the SPInS package
import model
import utilities

# provide a way to deactivate tqdm, if SPInS is running in batch mode
# or if tqdm is not available:
if (config.batch):
    def tqdm(x,total=None):
        return x
else:
    from tqdm import tqdm

# recreate np.int and np.float aliases if need be
if (not hasattr(np, "float")): np.float = float
if (not hasattr(np, "int")):   np.int = int

# parameters associated with the grid
grid             = None   
""" grid of models """

grid_params_MCMC = ()     
""" parameters used in the MCMC run (dimension = ndims) """

grid_params_MCMC_fundamental = ()     
""" fundamental parameters used in the MCMC run """

grid_params_MCMC_index = ()     
""" array with indices of the parameters used in the MCMC run """

output_params_index = ()
""" array with indices of supplementary output parameters """

output_params_funcs = ()
""" tuple with functions applied to supplementary output parameters """

ndims            = 0      
""" number of dimensions for MCMC parameters per star """

ndims_total      = 0
""" total number of dimensions for MCMC parameters """

ncommon          = 0
""" number of common parameters when fitting multiple stars """

# parameters associated with input stars
nstars           = 0
""" number of stars being fitted simultaneously """

star_names       = []
""" the names of the stars being fitted """

# parameters associated with probabilities
prob      = None          
"""
:py:class:`Probability` type object that represents the probability function
which includes the likelihood and priors
"""

initial_distributions = None
"""
:py:class:`Prior_list` type object which stores the distributions for the initial 
tight ball used to initialise the MCMC run (provided :py:data:`tight_ball`==``True``).
"""

log0      = -1e300        
""" a large negative value used to represent ln(0) """

threshold = -1e290        
""" threshold for "accepted" models.  Needs to be greater than :py:const:`log0` """

# variables associated with the best parameters from a scan of the parameter space:
best_grid_params  = None  
""" best parameters after scanning the grid (once per star) """

best_grid_result  = log0  
""" ln(probability) obtained for :py:data:`best_grid_params`"""

# variables associated with the best model from the MCMC run:
best_MCMC_params  = None
""" best parameters from the MCMC run """

best_MCMC_result  = log0
""" best ln(probability) result from the MCMC run """

# variables associated with the statistical parameters
statistical_params= None
""" statistical parameters obtained from averaging the samples """

statistical_result= log0
""" ln(probability) result for :py:data:`statistical_params`"""

# other parameters
output_folder = None
""" folder in which to write the results """

labels        = None
""" labels for plots """

pool          = None
""" pool from which to carry out parallel computations """

my_map        = None      
""" pointer to the map function (either the parallel or sequential versions) """

naccepted_parameters = []
""" number of sets of parameters associated with accepted models for each star """

nrejected_parameters = []
""" number of sets of parameters associated with rejected models for each star """

autocorr_time = []
""" integrated autocorrelelation time (this is useful for testing convergence) """

converged = None
""" indicates if the mcmc run has converged (True) or not (False); None = convergence has not been tested  """

count = 0
""" if the mcmc has converged before reaching the maximum nsteps, it provides the step at which
it has already reached convergence
"""

mc2 = None
""" True if emcee's version is 2 or prior """

class Distribution:

    """
    A class which represents a probability distribution, and can yield
    its value for a given input parameter, or provide a random realisation.
    
    .. note::
      Derived from a class originally written by G. Davies.
    """

    def __init__(self, _type, _values):
        """
        :param _type: type of probability function (current options include
                       "Uniform", "Gaussian", "Truncated_gaussian", "IMF1",
                       "IMF2", "Above", "Below", "Uninformative", or "Casa2011")
        :type _type: string

        :param _values: list of parameters relevant to the probability function

        :type _values: list of floats
        """

        self.type = _type
        """
        Type of probability function ("Uniform", "Gaussian", "Truncated_gaussian",
        "IMF1", "IMF2", "Above", "Below", "Uninformative", or "Casa2011")
        """
        
        self.values = _values
        """List of parameters relevant to probability function"""

    def __call__(self, value):
        """
        Return ln of probability distribution function for a particular value.
        
        :param value: value at which to evaluate ln of probability distribution function.
        :type value: float

        :return: ln(distribution(value))
        :rtype: float

        .. note::
          The results are given to within an additive constant.
        """

        if self.type == "Gaussian":
            return -((value - self.values[0])/self.values[1])**2 / 2.0
        elif self.type == "Truncated_gaussian":
            if (abs(value - self.values[0]) < self.values[1]*self.values[2]):
                return -((value - self.values[0])/self.values[1])**2 / 2.0
            else:
                return log0
        elif self.type == "Uniform":
            if value < self.values[0] or value > self.values[1]:
                return log0
            else:
                return 0.0
        elif self.type == "IMF1":
            # values[0] < values[1] = mass limits
            # values[2] = exponent
            if (self.values[0] <= value <= self.values[1]):
                return -self.values[2]*math.log(value/self.values[0])
            else:
                return log0
        elif self.type == "IMF2":
            # values[0] < values[1] < values[2] = mass limits
            # values[3], values[4] = exponents
            if (self.values[0] <= value <= self.values[1]):
                return -self.values[3]*math.log(value/self.values[0])
            elif (self.values[1] < value <= self.values[2]):
                return -self.values[3]*math.log(self.values[1]/self.values[0]) \
                       -self.values[4]*math.log(value/self.values[1])
            else:
                return log0
        elif self.type == "Above":
            if (value >= self.values[0]):
                return 0.0
            else:
                return log0
        elif self.type == "Below":
            if (value <= self.values[0]):
                return 0.0
            else:
                return log0
        elif self.type == "Uninformative":
            return 0.0
        elif self.type == "Casa2011":
            log_mMH=-(value-0.04)**2./(2.*0.12**2.)
            if (value >= 0.04):
                return math.log(387.8)+log_mMH
            else:
                mMH_corMH=math.exp(log_mMH)+0.3*(math.exp(-20.*(value+0.26)+log_mMH)-math.exp(-6.0+log_mMH))
                return math.log(387.0*mMH_corMH + 0.8)
        else:
            sys.exit("Unrecognised distribution: "+self.type)

    def realisation(self, size=None):
        """
        Return random values which statistically follow the probability distribution.

        :param size: shape of random variates (NOTE: ``None`` corresponds to 1 value)
        :type size: int or tuple of ints

        :return: a set of random realisations
        :rtype: float
        """

        if self.type == "Gaussian":
            return np.random.normal(self.values[0],self.values[1],size=size)
        elif self.type == "Truncated_gaussian":
            return truncnorm.rvs(-self.values[2],self.values[2],loc=self.values[0], \
                                 scale=self.values[1],size=size)
        elif self.type == "Uniform":
            return np.random.uniform(self.values[0],self.values[1],size=size)
        elif self.type == "IMF1":
            cnst2 = self.values[0]**(1.0-self.values[2])
            cnst1 = self.values[1]**(1.0-self.values[2]) - cnst2
            return (cnst1*np.random.uniform(0.0,1.0,size=size)+cnst2)**(1.0/(1.0-self.values[2]))
        elif self.type == "IMF2":
            rlim  = 1.0/(1.0 + ((1.0-self.values[3])/(1.0-self.values[4]))                       \
                  * self.values[1]**(self.values[4]-self.values[3])                              \
                  * (self.values[2]**(1.0-self.values[4])-self.values[1]**(1.0-self.values[4]))  \
                  / (self.values[1]**(1.0-self.values[3])-self.values[0]**(1.0-self.values[3])))
            cnst2 = self.values[0]**(1.0-self.values[3])
            cnst1 = (self.values[1]**(1.0-self.values[3]) - cnst2)/rlim
            cnst4 = self.values[1]**(1.0-self.values[4])
            cnst3 = (self.values[2]**(1.0-self.values[4]) - cnst4)/(1.0-rlim)
            def Finv(r):
                if (r <= rlim):
                    return (cnst1*r+cnst2)**(1.0/(1.0-self.values[3]))
                else:
                    return (cnst3*(r-rlim)+cnst4)**(1.0/(1.0-self.values[4]))
            vFinv = np.vectorize(Finv)    
            return vFinv(np.random.uniform(0.0,1.0,size=size))
        elif self.type == "Above":
            sys.exit("Unable to produce a realisation for an \"Above\" distribution")
        elif self.type == "Below":
            sys.exit("Unable to produce a realisation for a \"Below\" distribution")
        elif self.type == "Uninformative":
            sys.exit("Unable to produce a realisation for an uninformative distribution")
        elif self.type == "Casa2011":
            sys.exit("Realisations for a \"Casa2011\" distribution not currently implemented")
        else:
            sys.exit("Unrecognised distribution: "+self.type)

    def re_centre(self,value):
        """
        Re-centre the probability distribution around the input value.

        :param value: new value around which to centre the distribution
        :type value: float
        """

        if self.type == "Gaussian":
            self.values[0] = value
        elif self.type == "Truncated_gaussian":
            self.values[0] = value
        elif self.type == "Uniform":
            dist = (self.values[1] - self.values[0])/2.0
            self.values[0] = value - dist
            self.values[1] = value + dist
        elif self.type == "IMF1":
            sys.exit("re_centre method not implemented for IMF1")
        elif self.type == "IMF2":
            sys.exit("re_centre method not implemented for IMF2")
        elif self.type == "Above":
            self.values[0] = value
        elif self.type == "Below":
            self.values[0] = value
        elif self.type == "Uninformative":
            # do nothing
            pass
        elif self.type == "Casa2011":
            # do nothing
            pass
        else:
            sys.exit("Unrecognised distribution: "+self.type)

    def re_normalise(self,value):
        """
        Re-normalise the probability distribution so that its characteristic
        width corresponds to the input value.

        :param value: new value around for the chacteristic width
        :type value: float
        """

        if self.type == "Gaussian":
            self.values[1] = value
        elif self.type == "Truncated_gaussian":
            self.values[1] = value
        elif self.type == "Uniform":
            centre = (self.values[0] + self.values[1])/2.0
            self.values[0] = centre - value
            self.values[1] = centre + value
        elif self.type == "IMF1":
            sys.exit("re_normalise method not implemented for IMF1")
        elif self.type == "IMF2":
            sys.exit("re_normalise method not implemented for IMF2")
        elif self.type == "Above":
            sys.exit("Unable to renormalise an \"Above\" distribution")
        elif self.type == "Below":
            sys.exit("Unable to renormalise a \"Below\" distribution")
        elif self.type == "Uninformative":
            sys.exit("Unable to renormalise an uninformative distribution")
        elif self.type == "Casa2011":
            sys.exit("Unable to renormalise a \"Casa2011\" distribution")
        else:
            sys.exit("Unrecognised distribution: "+self.type)

    @property
    def mean(self):
        """
        Returns the mean value of the probability distribution.

        :return:  the mean value of the probability distribution
        :rtype: float
        """
        if self.type == "Gaussian":
            return self.values[0]
        elif self.type == "Truncated_gaussian":
            return self.values[0]
        elif self.type == "Uniform":
            return (self.values[0] + self.values[1])/2.0
        elif self.type == "IMF1":
            return ((1.0-self.values[2]) / (2.0-self.values[2]))  \
              * (self.values[1]**(2.0-self.values[2])-self.values[0]**(2.0-self.values[2])) \
              / (self.values[1]**(1.0-self.values[2])-self.values[0]**(1.0-self.values[2]))
        elif self.type == "IMF2":
            k = 1.0/((self.values[1]**(1.0-self.values[3])-self.values[0]**(1.0-self.values[3])) \
              / ((1.0-self.values[3])*self.values[0]**(-self.values[3]))                         \
              + (self.values[1]/self.values[0])**(-self.values[3])                               \
              * (self.values[2]**(1.0-self.values[4])-self.values[1]**(1.0-self.values[4]))      \
              / ((1.0-self.values[4])*self.values[1]**(-self.values[4])))
            return k*((self.values[1]**(2.0-self.values[3])-self.values[0]**(2.0-self.values[3])) \
              / ((2.0-self.values[3])*self.values[0]**(-self.values[3]))                         \
              + (self.values[1]/self.values[0])**(-self.values[3])                               \
              * (self.values[2]**(2.0-self.values[4])-self.values[1]**(2.0-self.values[4]))      \
              / ((2.0-self.values[4])*self.values[1]**(-self.values[4])))
        elif self.type == "Above":
            return np.nan
        elif self.type == "Below":
            return np.nan
        elif self.type == "Uninformative":
            return np.nan
        elif self.type == "Casa2011":
            return 0.04  # approximately true
        else:
            sys.exit("Unrecognised distribution: "+self.type)

    @property
    def error_bar(self):
        """
        Returns an error bar based on the distribution.  This
        does not necessarily correspond to the one-sigma value
        but rather to what is the most convenient value.

        :return: the error bar
        :rtype: float
        """
        if self.type == "Gaussian":
            return self.values[1]
        elif self.type == "Truncated_gaussian":
            return self.values[1]
        elif self.type == "Uniform":
            return (self.values[1] - self.values[0])/2.0
        elif self.type == "IMF1":
            k = (1.0-self.values[2]) \
              / (self.values[1]**(1.0-self.values[2])-self.values[0]**(1.0-self.values[2]))
            mean1 = (self.values[1]**(2.0-self.values[2])-self.values[0]**(2.0-self.values[2])) \
              * k / (2.0-self.values[2])
            mean2 = (self.values[1]**(3.0-self.values[2])-self.values[0]**(3.0-self.values[2])) \
              * k / (3.0-self.values[2])
            return math.sqrt(mean2 - mean1*mean1)
        elif self.type == "IMF2":
            k = 1.0/((self.values[1]**(1.0-self.values[3])-self.values[0]**(1.0-self.values[3])) \
              / ((1.0-self.values[3])*self.values[0]**(-self.values[3]))                         \
              + (self.values[1]/self.values[0])**(-self.values[3])                               \
              * (self.values[2]**(1.0-self.values[4])-self.values[1]**(1.0-self.values[4]))      \
              / ((1.0-self.values[4])*self.values[1]**(-self.values[4])))
            mean1 = k*((self.values[1]**(2.0-self.values[3])-self.values[0]**(2.0-self.values[3])) \
              / ((2.0-self.values[3])*self.values[0]**(-self.values[3]))                         \
              + (self.values[1]/self.values[0])**(-self.values[3])                               \
              * (self.values[2]**(2.0-self.values[4])-self.values[1]**(2.0-self.values[4]))      \
              / ((2.0-self.values[4])*self.values[1]**(-self.values[4])))
            mean2 = k*((self.values[1]**(3.0-self.values[3])-self.values[0]**(3.0-self.values[3])) \
              / ((3.0-self.values[3])*self.values[0]**(-self.values[3]))                         \
              + (self.values[1]/self.values[0])**(-self.values[3])                               \
              * (self.values[2]**(3.0-self.values[4])-self.values[1]**(3.0-self.values[4]))      \
              / ((3.0-self.values[4])*self.values[1]**(-self.values[4])))
            return math.sqrt(mean2 - mean1*mean1)
        elif self.type == "Above":
            return np.nan
        elif self.type == "Below":
            return np.nan
        elif self.type == "Uninformative":
            return np.nan
        elif self.type == "Casa2011":
            return 0.12  # approximately true
        else:
            sys.exit("Unrecognised distribution: "+self.type)

    @property
    def nparams(self):
        """
        Return the number of relevant parameters for a given distribution.

        :return: the number of relevant parameters
        :rtype: int
        """
        if self.type == "Gaussian":
            return 2
        elif self.type == "Truncated_gaussian":
            return 3
        elif self.type == "Uniform":
            return 2
        elif self.type == "IMF1":
            return 3
        elif self.type == "IMF2":
            return 5
        elif self.type == "Above":
            return 1
        elif self.type == "Below":
            return 1
        elif self.type == "Uninformative":
            return 0
        elif self.type == "Casa2011":
            return 0
        else:
            return 0

    def print_me(self):
        """Print type and parameters of probability distribution."""
        
        print(self.type + " " + str(self.values[0:self.nparams]))

    def to_string(self):
        """
        Produce nice string representation of the distribution.

        :return: nice string representation of the distribution
        :rtype: string
        """
        return self.type + " " + str(self.values[0:self.nparams])
        
class Prior_list:
    """
    A class which contains a list of priors as well as convenient methods for
    adding priors and for evaluating them.
    """

    def __init__(self, prior_dict):
        self.priors = [None,]*ndims
        """
        A list of priors or tight ball distributions.  Each contain the following
        items (in this order - the numbers correspond to the indices):

          0. the name of the parameter (including functions applied to the parameter).
          1. a list of functions (not strings) which when applied to the corresponding
             MCMC parameter produces the parameter in item 0.
          2. the derivatives of the functions in item 1.  These derivatives are necessary
             for correctly calculating the probability in terms of the corresponding MCMC
             parameter rather than the parameter given in item 0.
          3. the inverted list of functions.  When applied to the parameter in item 0,
             it produces one of the MCMC parameters.
          4. a probability distribution.
          5. a multiplicative coefficient on ln(prior) to account for common parameters
             when fitting multiple stars.
        """

        self.init_priors(prior_dict)

    def init_priors(self,prior_dict):
        """
        Initialise the priors using a python dictionary with the name
        of the parameters as keys, and distributions as values.

        :param prior_dict: dictionary with parameters as keys and distributions
                           as values
        :type prior_dict:  {str : :py:class:`Distribution`} dictionary
        """

        for name in prior_dict:
            if (name.lower().endswith("_multi")):
                name_simple = model.simplify_name(name[:-6])
                coef = 1.0
            else:
                name_simple = model.simplify_name(name)
                if (name_simple not in config.common_params):
                    coef = 1.0
                else:
                    coef = 1.0/nstars
            fundamental_parameter = model.extract_fundamental_parameter(name_simple)
            if (fundamental_parameter not in grid_params_MCMC_fundamental): continue
            i = grid_params_MCMC_fundamental.index(fundamental_parameter)
            if (self.priors[i] is not None):
                sys.exit("Multiple priors or tight-ball ranges on parameter %s. Aborting"%( \
                         fundamental_parameter))
            self.priors[i] = (name_simple,)+grid.string_to_param_extended(name_simple)[1:] \
                           + (Distribution(*prior_dict[name]),coef)

    def realisation(self,size=None):
        """
        Return an array with realisations for each prior.  The last dimension
        will correspond to the different priors.

        :param size: shape of random variates (for each prior)
        :type size: int or tuple of ints

        :return: a set of realisations
        :rtype: numpy float array

        .. note::
           Realisations of priors do not take into account the multiplicative coefficient
           of common parameters for multiple stars as a single realisation will be used
           for all of the stars.
        """

        if (size is None):
            return np.array([model.apply_functions(prior[3],prior[4].realisation(size=size)) \
                             for prior in self.priors])
        else:
            if (type(size) == int):
                output_size = (size,1)
            else:
                output_size = size + (1,)
            result = [np.reshape(model.apply_functions(prior[3],prior[4].realisation(size=size)), \
                             output_size) for prior in self.priors]
            return np.concatenate(result,axis=-1)

    def __call__(self, params):
        """
        Evaluate ln of priors for a list of parameters.

        :param params: list of parameters which intervene in the priors.
        :type params: array-like

        :return: ln(prior probability)
        :rtype: float

        .. warning::        
          The list of parameters should contain the same number of elements
          as the number of priors.
        """

        assert (len(params) == len(self.priors)), "Incorrect number of parameters in call to Log_prior"
        lnprior = 0.0
        for (prior, param) in zip(self.priors, params):
            fvalue, fder = model.apply_functions_der(prior[1],prior[2],param)
            lnprior += prior[5]*(prior[4](fvalue) + math.log(abs(fder)))
        return lnprior

    def print_me(self):
        """
        Print contents of prior list, including the names of the parameters, the
        functions applied to these and the distributions.
        """
        for i in range(ndims):
            print("Prior on %s: %s %f %s"%(self.priors[i][0], self.priors[i][4].to_string(), \
                  self.priors[i][5], str([f.__name__ for f in self.priors[i][1]])))

class Likelihood:
    """
    A class which describes the likelihood function and allows users to evaluate it.
    """

    def __init__(self):
        self.constraints = []
        """List of constraints which intervene in the likelihood function."""

    def read_constraints(self,filename):
        """
        Read a file with constraints.
        
        :param filename: name of file with constraints
        :type filename: string
        """
        
        obsfile = open(filename,"r")
        for line in obsfile:
            line = utilities.trim(line.strip())  # remove comments
            columns = line.split()
            if (len(columns) < 3): continue
           
            # detect whether the type of distribution is specified or not
            if (utilities.is_number(columns[1])):
                like.add_constraint((columns[0],           \
                    Distribution("Gaussian",               \
                    utilities.my_map(utilities.to_float,columns[1:]))))
            else:
                like.add_constraint((columns[0],           \
                    Distribution(columns[1],               \
                    utilities.my_map(utilities.to_float,columns[2:]))))

        # don't forget to do this:
        obsfile.close()

    def add_constraints_from_prior(self,prior_dict):
        """
        Extract constraints from a priors dictionary with the name
        of the parameters as keys, and distributions as values.

        :param prior_dict: dictionary with parameters as keys and distributions
                           as values
        :type prior_dict:  {str : :py:class:`Distribution`} dictionary
        """

        for name in prior_dict:
            fundamental_parameter = model.extract_fundamental_parameter(name)

            # skip true priors (these will be dealt with using a Prior_list object)
            if (fundamental_parameter in grid_params_MCMC_fundamental): continue 

            # add the corresponding constraint
            self.add_constraint((name,Distribution(*prior_dict[name])))

    def add_constraint(self,a_constraint):
        """
        Add a supplementary constraint to the list of constraints.
        
        :param constraint: supplementary constraint
        :type constraint: (string, :py:class:`Distribution`)
        """

        (name,distribution) = a_constraint

        self.constraints.append((model.simplify_name(name),) \
            +grid.string_to_param(name)+(distribution,))

    def evaluate(self, my_model):
        """
        Calculate ln of likelihood function (i.e. a :math:`\chi^2` value multiplied by
        -1/2) for a given model.
        
        :param my_model: model for which the :math:`\chi^2` value (multiplied by -1/2)
                         is being calculated
        :type my_model: :py:class:`model.Model`

        :return: the :math:`\chi^2` value (multiplied by -1/2), i.e. ln(likelihood)
        :rtype: float

        .. note::
           * This avoids model interpolation and can be used to gain time.
           * This includes a correction to account for the use of a non-dimensional
             age parameter
        """

        chi2 = 0.0
        for constraint in self.constraints:
            (name, ndx, funcs, distrib) = constraint
            chi2 += distrib(model.apply_functions(funcs,my_model[ndx]))
        return chi2

    def __call__(self, params):
        """
        Calculate ln of likelihood function (i.e. a :math:`\chi^2` value multiplied by
        -1/2) for a given set of parameters.
        
        :param params: set of parameters for which the :math:`\chi^2` value (multiplied
                       by -1/2) is being calculated.
        :type params: array-like

        :return: the :math:`\chi^2` value (multiplied by -1/2), i.e. ln(likelihood)
        :rtype: float
        """

        if (params is None): return log0
        my_model = model.interpolate_model(grid,params[0:ndims],grid.tessellation,grid.ndx)
        if (my_model is None): return log0
        result = self.evaluate(my_model)
        del my_model  # avoid memory leaks
        return result

class Probability:
    """
    A class which combines the priors and likelihood function, and allows the
    the user to evalute ln of the product of these.
    """

    def __init__(self,_priors,_likelihoods):
        """
        :param _priors: input set of priors
        :param _likelihood: input likelihood function

        :type _priors: :py:class:`Prior_list`
        :type _likelihood: :py:class:`Likelihood`
        """

        self.priors = _priors
        """The set of priors (the same priors apply to each star)."""

        self.likelihoods = _likelihoods
        """The likelihood functions (one for each star)."""

        self.param_map = find_param_map()

    def evaluate(self, my_model, star_index):
        """
        Evalulate the ln of the product of the priors and likelihood function,
        i.e. the probability, for a given model and a given star, to within an
        additive constant.
        
        :param my_model: input model
        :type my_model: :py:class:`model.Model`

        :param star_index: index of star for which we would like to evaluate the
                           probability
        :type star_index: int

        :return: the ln of the probability
        :rtype: float

        .. note::
          This avoids model interpolation and can be used to gain time.
        """

        result1 = self.likelihoods[star_index].evaluate(my_model)
        result2 = self.priors(my_model[grid_params_MCMC_index])

        return result1 + result2

    def __call__(self, params):
        """
        Evalulate the ln of the product of the priors and likelihood functions,
        i.e. the probability, for a given set of parameters (corresponding to a
        set of models, one per observed stars), to within an additive constant.

        :param params: input set of parameters
        :type params: array-like
        
        :return: the ln of the probability
        :rtype: float
        """

        result1 = 0.0
        result2 = 0.0

        for i in range(nstars):
            star_params = params[self.param_map[i]]
            result1 += self.likelihoods[i](star_params)
            result2 += self.priors(star_params)

        return result1 + result2

    def is_outside(self, params):
        """
        Test to see if the given set of parameters lies outside the grid of
        models.  This is done by evaluating the probability and seeing if
        the result indicates this.
        
        :param params: input set of parameters
        :type params: array-like

        :return: ``True`` if the set of parameters corresponds to a point
           outside the grid.
        :rtype: boolean
        """

        # the following condition is "nan-resistant":
        return not (self(params) >= threshold)

    def like_function(self,params):
        """
        Evaluate the combined likelihood function for all of the stars.

        :param params: input set of parameters
        :type params: array-like
        
        :return: the ln of the likelihood
        :rtype: float
        """

        result = 0.0
        for i in range(nstars):
            result += self.likelihoods[i](params[self.param_map[i]])

        return result

    def prior_function(self,params):
        """
        Evaluate the combined prior function for all of the stars.

        :param params: input set of parameters
        :type params: array-like
        
        :return: the ln of the prior
        :rtype: float
        """

        result = 0.0
        for i in range(nstars):
            result += self.priors(params[self.param_map[i]])

        return result

def like_function(params):
    """
    Wrapper function which allows prob.like_function to be pickled
    and thus used in a parallelised context.

    :param params: input set of parameters
    :type params: array-like

    :return: the ln of the likelihood
    :rtype: float
    """

    return prob.like_function(params)

def prior_function(params):
    """
    Wrapper function which allows prob.prior_function to be pickled
    and thus used in a parallelised context.

    :param params: input set of parameters
    :type params: array-like

    :return: the ln of the prior
    :rtype: float
    """

    return prob.prior_function(params)

def find_param_map():
    """
    Finding mapping between input set of parameters and parameters
    for each star, taking into account the fact that some of the
    parameters may be common parameters (such as the age for a
    binary system/cluster)

    :return: the mapping from the reduced to the extended set of
             parameters
    :rtype: a 2D int array
    """

    param_map = np.zeros((nstars,ndims),dtype=int)-1

    # deal with common parameters
    for elt in config.common_params:
        i = grid_params_MCMC_fundamental.index(elt)
        param_map[:,i] = i

    # deal with first star:
    for j in range(ndims):
        param_map[0,j] = j

    # deal with remaining stars
    k = ndims
    for i in range(1,nstars):
        for j in range(ndims):
            if (param_map[i,j] == -1):
                param_map[i,j] = k
                k += 1

    return param_map

 
def check_configuration():
    """
    Test the version of the EMCEE package and set the mc2 variable accordingly.

    Test the values of the variables in check_configuration to make
    sure they're acceptable.  If an unacceptable value is found, then
    this will stop SPInS and explain what variable has an erroneous
    value.
    """

    global mc2
    emcee_version = int(emcee.__version__.split(".")[0])
    mc2 = (emcee_version < 3)

    assert ((config.nwalkers%2) == 0), "nwalkers should be even.  Please modify SPInS_configure.py"
    for i in range(len(config.common_params)-1):
        for j in range(i+1,len(config.common_params)):
            assert(config.common_params[i] != config.common_params[j]), \
                "Found duplicate entry in common_params.  Please modify SPInS_configure.py"
    return

def write_binary_data(infile,outfile):
    """
    Read an ascii file with a grid of models, and write corresponding binary file.
    
    :param infile: input ascii file name
    :type infile: string

    :param outfile: output binary file name
    :type outfile: string
    """

    grid = model.Model_grid()
    grid.read_model_list(infile)
    if (config.distort_grid):
        grid.distort_grid()
    grid.tessellate()
    output = open(outfile,"wb")
    dill.dump(grid,output)
    output.close()
    grid.plot_tessellation()

def load_binary_data(filename):
    """
    Read a binary file with a grid of models.
    
    :param filename: name of file with grid in binary format
    :type filename: string

    :return: the grid of models
    :rtype: :py:class:`model.Model_grid`
    """

    input_data = open(filename,"rb")
    grid = dill.load(input_data)
    input_data.close()
    retessellate = False
    if (config.distort_grid):
        if (hasattr(grid,"distort_mat")):
            if (grid.distort_mat is None):
                grid.distort_grid()
                retessellate = True
            else:
                retessellate = False
        else:
            grid.distort_grid()
            retessellate = True
    else:
        if (hasattr(grid,"distort_mat")):
            if (grid.distort_mat is None):
                retessellate = False
            else:
                grid.distort_mat = None
                retessellate = True
        else:
            grid.distort_mat = None
            retessellate = False

    if (retessellate or config.retessellate):
        print("Re-tessellating grid")
        grid.tessellate()
    model.nglb = len(grid.all_params)

    return grid

def find_best_models():
    """
    Find best parameters for each model treated individually then combine
    parameters to obtain a global set of optimal parameters.  If there are
    common parameters, these are averaged between the different solutions.

    :return: the age ranges for the evolutionary tracks with the best models,
             and the common age range
    :rtype: 1D numpy float array, float
    """

    global best_grid_params, best_grid_result
    global naccepted_parameters, nrejected_parameters

    naccepted_parameters = np.empty((nstars,),dtype=int)
    nrejected_parameters = np.empty((nstars,),dtype=int)
    best_grid_params = np.zeros((ndims_total,),dtype=model.gtype)
    all_best_params  = np.empty((nstars,ndims),dtype=model.gtype)
    age_ranges       = np.empty((nstars,2),dtype=model.gtype)
    common_age_range = np.empty((2,),dtype=model.gtype)

    for i in range(nstars):
        best_params, naccepted, nrejected = find_best_model(i)
        all_best_params[i]      = best_params[:ndims]
        naccepted_parameters[i] = naccepted
        nrejected_parameters[i] = nrejected

    # the following set of parameters (excluding age) is garanteed to be within
    # the convex hull to the grid because of its convexity:
    avg_best_params = np.average(all_best_params,axis=0)

    # construct set of best parameters for each star by replacing common
    # elements by the average value:
    for elt in config.common_params:
        j = grid_params_MCMC_fundamental.index(elt)
        all_best_params[:,j] = avg_best_params[j]

    # check to see if resultant structural parameters are in the convex
    # hull of the grid.  If not, find linear combination of avg_best_params
    # and all_best_params[i] which is within the convex hull and use that.
    for i in range(nstars):
        all_best_params[i,:-1] = model.find_point_in_convex_hull(all_best_params[i,:-1], \
                                 avg_best_params[:ndims-1],grid.tessellation,grid.distort_mat)

    # map best parameters from individual stars on global best parameter array:
    for i in range(nstars):
        for j in range(ndims):
            best_grid_params[prob.param_map[i,j]] = all_best_params[i,j]

    # find age ranges for above set of best parameters
    for i in range(nstars):
        age_ranges[i] = grid.age_range(all_best_params[i])
    common_age_range[0] = np.max(age_ranges[:,0])
    common_age_range[1] = np.min(age_ranges[:,1])

    # if age is a common parameter, check to see if it is in the common age range
    if (model.age_str in config.common_params):
        # sanity check:
        if (common_age_range[0] > common_age_range[1]):
            print("ERROR: the adjusted best-fitting stellar evolution tracks for the")
            print("       observed stars do not have a common age interval.  Aborting.")
            sys.exit(1)
        i = grid_params_MCMC_fundamental.index(model.age_str)
        print("Common age (Myrs): %f"%(best_grid_params[i]))
        print("Age range (Myrs):  %f - %f"%(common_age_range[0],common_age_range[1]))
        if (not (common_age_range[0] <= best_grid_params[i] <= common_age_range[1])):
            if (best_grid_params[i] < common_age_range[0]):
                best_grid_params[i] = common_age_range[0]
                sbound = "lower"
            else:
                best_grid_params[i] = common_age_range[1]
                sbound = "upper"
            print("WARNING: the best-fitting common age lies outside the common age")
            print("         interval.  Replacing this common age by the %s bound."%(sbound))

    # find associated probability:
    best_grid_result = prob(best_grid_params)

    # sanity check:
    if (best_grid_result < threshold):
        print("ERROR: for an unexpected reason, SPInS was unable to find a reasonable")
        print("       set of initial parameters for your set of stars.  Please contact")
        print("       the authors of SPInS.  Alternatively, try running SPInS with")
        print("       \"tight_ball = False\" in SPInS_configure.py")
        sys.exit(1)

    # print results
    print("Best overall parameters: %g %s"%(best_grid_result, str(best_grid_params)))
    return age_ranges[:,1]-age_ranges[:,0], common_age_range[1]-common_age_range[0]

def find_best_model(star_index):
    """
    Scan through grid of models to find "best" model for a given probability
    function for an individual star (i.e. the product of priors and a likelihood
    function).

    :param star_index: index of star being fitted
    :type star_index: int

    :return: the best model for the star, the number of accepted parameters (i.e. models),
             the number of rejected parameters (i.e. models)
    :rtype: float array, int, int
    """

    # find best models in each track, in a parallelised way:
    aux = zip(range(len(grid.tracks)),(star_index,)*len(grid.tracks))
    results = my_map(find_best_model_in_track,aux)

    best_result    = log0
    best_model     = None
    accepted_parameters = []
    rejected_parameters = []
    for (result,my_model,accepted,rejected) in results:
        accepted_parameters += accepted
        rejected_parameters += rejected
        if (result > best_result):
            best_result    = result
            best_model     = my_model

    if (best_model is None):
        print("ERROR:  Unable to find a model in the grid which matches the")
        print("        constraints for %s.  Try extending your grid."%(star_names[star_index]))
        print("        Aborting.")
        sys.exit(1)

    # Output various results (doing this here avoid us having to store
    # the best parameters of each individual model):
    prefix = ""
    if (nstars > 1): prefix = star_names[star_index]+"_"

    best_params  = list(best_model[grid_params_MCMC_index]) \
                      + find_output_params(best_model)
    if config.with_models:
        write_model(best_model,best_params,best_result,np.nan,prefix+"best_grid")
    print("Best model for %s: %g %s"%(star_names[star_index], best_result, \
          str(best_model[grid_params_MCMC_index])))

    if (config.with_rejected):
        if (len(rejected_parameters) >= ndims):
            try:
                fig = corner.corner(np.array(rejected_parameters), labels=labels[1:ndims+1])
                for ext in config.tri_extensions:
                    fig.savefig(os.path.join(output_folder,prefix+"rejected."+ext))
                    plt.close('all')
            except ValueError:
                pass
        if (len(accepted_parameters) >= ndims):
            try:
                fig = corner.corner(np.array(accepted_parameters), labels=labels[1:ndims+1])
                for ext in config.tri_extensions:
                    fig.savefig(os.path.join(output_folder,prefix+"accepted."+ext))
                    plt.close('all')
            except ValueError:
                pass

    return best_params, len(accepted_parameters), len(rejected_parameters)

def find_best_model_in_track(track_star):
    """
    Scan through an evolutionary track to find "best" model for :py:data:`prob`,
    the probability function (i.e. the product of priors and a likelihood function)
    for a given observed star.
    
    :param track_star: number of the evolutionary track and index of the observed star.
    :type track_star: (int, int)

    :return: the ln(probability) value, and the "best" model, the parameters of the
             accepted models, and those of the rejected models
    :rtype: float, :py:class:`model.Model`, tuple of float arrays, tuple of float arrays
    """

    (ntrack,star_index) = track_star

    best_result_local = log0
    best_model_local  = None
    rejected_parameters_local = []
    accepted_parameters_local = []
    for i in range(grid.tracks[ntrack].models.shape[0]):
        my_model = grid.tracks[ntrack].models[i,:]
        result = prob.evaluate(my_model,star_index)
        if (result > best_result_local):
            best_result_local = result
            best_model_local  = my_model
        if (result >= threshold):
            accepted_parameters_local.append(my_model[grid_params_MCMC_index])
        else:
            rejected_parameters_local.append(my_model[grid_params_MCMC_index])

    return (best_result_local, best_model_local, accepted_parameters_local, rejected_parameters_local)

def init_walkers():
    """
    Initialise the walkers used in emcee.

    :return: array of starting parameters
    :rtype: np.array
    """

    # set up initial distributions according to the value of config.tight_ball:
    global initial_distributions

    if (config.samples_file is None):

        if (config.tight_ball):

            # create probability distributions for a tight ball:
            # (just reuse the same class as for the priors)
            initial_distributions = []
            for j in range(nstars):
                initial_distributions.append(Prior_list(config.tight_ball_range))
            for i in range(ndims):
                if (initial_distributions[0].priors[i] is None):
                    param_range = grid.range(grid_params_MCMC[i])
                    param_range = (param_range[1]-param_range[0])/30.0
                    for j in range(nstars):
                        initial_distributions[j].priors[i] = (grid_params_MCMC[i],[],[],[], \
                            Distribution("Gaussian",[0.0,param_range]),1.0)

            # find the best models:
            age_ranges, common_age_range = find_best_models()
            age_ranges /= 30.0
            if (model.age_str in config.common_params):
                cnst = common_age_range/30.0
                age_ranges[:] = cnst

            # recentre tight ball around best model:
            # Don't forget the parameter conversion!
            tight_ball_fundamental = utilities.my_map(model.extract_fundamental_parameter, \
                                     config.tight_ball_range.keys())
            if (config.tight_ball_behaviour.lower() == "keep"):
                for j in range(nstars):
                    for i in range(ndims):
                        if (grid_params_MCMC_fundamental[i] in tight_ball_fundamental): continue
                        initial_distributions[j].priors[i][4].re_centre(model.apply_functions( \
                            initial_distributions[j].priors[i][1],best_grid_params[prob.param_map[j,i]]))
                        if (model.age_str == grid_params_MCMC_fundamental[i]):
                            initial_distributions[j].priors[i][4].re_normalise(age_ranges[j])
            elif ((config.tight_ball_behaviour.lower() == "recenter") or \
                  (config.tight_ball_behaviour.lower() == "recentre")):
                for j in range(nstars):
                    for i in range(ndims):
                        initial_distributions[j].priors[i][4].re_centre(model.apply_functions( \
                            initial_distributions[j].priors[i][1],best_grid_params[prob.param_map[j,i]]))
                        if ((model.age_str == grid_params_MCMC_fundamental[i]) and
                            (grid_params_MCMC_fundamental[i] not in tight_ball_fundamental)):
                            initial_distributions[j].priors[i][4].re_normalise(age_ranges[j])
            else:
                sys.exit('Unrecognised value of "tight_ball_behaviour" in SPInS_configure.py')

        else:

           # set the initial distributions to the priors
           # NOTE: the following simply makes multiple links to the same
           #       set of priors rather than cloning them.  Modifying the
           #       priors for one star will modify them for all of the stars.
           initial_distributions = (prob.priors,)*nstars

        # print initial distributions:
        for j in range(nstars):
            print("\n***** Initial distribution on star: %s *****"%(star_names[j]))
            initial_distributions[j].print_me()

        params = np.empty((ndims_total,),dtype=model.gtype)
        if (config.PT):
            p0 = np.zeros([config.ntemps, config.nwalkers, ndims_total])

            counter_total = 0 
            for k in range(config.ntemps):
                for j in range(config.nwalkers):
                    is_outside = True
                    counter = 0
                    while (is_outside):
                        if (counter > config.max_iter):
                            sys.exit("ERROR: too many iterations to produce walker.  Aborting")
                        # one should not average the realisations on common parameters,
                        # but simply select one of them:
                        for l in range(nstars):
                            params[prob.param_map[l]] = initial_distributions[l].realisation()
                        is_outside = prob.is_outside(params)
                        counter+=1
                    p0[k,j,:] = params
                    counter_total += counter

            # include the parameters of the best model as the first walker:
            if (config.tight_ball): p0[0,0] = best_grid_params

            print("Average number of iterations in init_walkers: %g\n"%(counter_total/float(config.ntemps*config.nwalkers)))

        else:
            p0 = np.zeros([config.nwalkers, ndims_total])
    
            counter_total = 0 
            for j in range(config.nwalkers):
                is_outside = True
                counter = 0
                while (is_outside):
                    if (counter > config.max_iter):
                        sys.exit("ERROR: too many iterations to produce walker.  Aborting")
                    # one should not average the realisations on common parameters,
                    # but simply select one of them:
                    for l in range(nstars):
                        params[prob.param_map[l]] = initial_distributions[l].realisation()
                    is_outside = prob.is_outside(params)
                    counter+=1
                p0[j,:] = params
                counter_total += counter

            # include the parameters of the best model as the first walker:
            if (config.tight_ball): p0[0] = best_grid_params

            print("Average number of iterations in init_walkers: %g\n"%(counter_total/float(config.nwalkers)))

    else:
        print("Initialise walkers from samples file")

        # NOTE: the first column contains the ln(P) values and must be discarded
        previous_samples = np.loadtxt(config.samples_file,usecols=range(1,ndims_total+1))
        if (len(previous_samples) == 1):
            previous_samples = previous_samples.reshape((1,ndims_total))
        nsamples = previous_samples.shape[0]

        # initiliase walkers by cycling backwards on previous samples in order
        # to use most "recent" samples first:
        if (config.PT):
            # NOTE: the initial distribution on higher temperatures will be the same
            #       as on lower temperatures, which is not representative of the
            #       the outcome from a multi-tempered MCMC run.
            p0 = np.zeros([config.ntemps, config.nwalkers, ndims_total])
            i = nsamples-1
            for k in range(config.ntemps):
                for j in range(config.nwalkers):
                    p0[k,j] = previous_samples[i]
                    i -= 1
                    if (i < 0): i = nsamples-1

        else:
            p0 = np.zeros([config.nwalkers, ndims_total])
            i = nsamples-1
            for j in range(config.nwalkers):
                p0[j] = previous_samples[i]
                i -= 1
                if (i < 0): i = nsamples-1 

    return p0

def run_emcee(p0):
    """
    Run the emcee program.

    :param p0: the initial set of walkers
    :type p0: np.array

    :return: the ``emcee`` sampler for the MCMC run, array with walker percentiles
             as a function of iteration number
    :rtype: emcee sampler object, np.array
    """

    print("***** Starting MCMC *****")
    print("Number of walkers:    %d"%(config.nwalkers))
    print("Number of steps:      %d"%(config.nsteps))

    global autocorr_time
    global converged
    global count
    
    if (config.PT):
        print("Number of temp.:      %d"%(config.ntemps))
        betas = ptemcee.sampler.default_beta_ladder(ndims_total, config.ntemps, Tmax=None)
        sampler = ptemcee.Sampler(config.nwalkers, ndims_total, like_function, \
                                  prior_function, betas=betas, pool=pool)
    else:
        sampler = emcee.EnsembleSampler(config.nwalkers, ndims_total, prob, pool=pool)

    # initialisation of percentiles:
    percentiles = []
    pvalues = (25.0,50.0,75.0)

    # initial burn-in:
    print("Burn-in iterations")
    if (config.PT):
        for p, lnprob, lnlike in tqdm(sampler.sample(p0, iterations = config.nsteps0, storechain=False, adapt=config.PTadapt),total=config.nsteps0):
            percentiles.append(np.percentile(p[0],pvalues,axis=0))
    else:
        if (mc2):
            for p, lnprob, lnlike in tqdm(sampler.sample(p0, iterations = config.nsteps0, storechain=False),total=config.nsteps0):
                percentiles.append(np.percentile(p,pvalues,axis=0))
        else:
            for sample in sampler.sample(p0, iterations = config.nsteps0, progress=not(config.batch), store=False):
                p = sample.coords
                percentiles.append(np.percentile(p,pvalues,axis=0))
    sampler.reset()

    # production run:
    print("Production iterations")
    steps = []
    if (config.PT):
        for p, lnprob, lnlike in tqdm(sampler.sample(p, iterations = config.nsteps, adapt=config.PTadapt),total=config.nsteps):
            percentiles.append(np.percentile(p[0],pvalues,axis=0))
            count += 1
            if count % config.nstepeval:
                continue
            tau = [autocorr_new(sampler.chain[0,:,:count,j]) for j in range(ndims_total)]
            autocorr_time.append(np.mean(tau))
            steps.append(count)
            if config.test_conv:
                converged = (np.mean(tau) * 100 < count)
                if (converged) & (count > config.nstepsmin):
                    break
            else:
                converged = None
    else:
        if (mc2):
            for p, lnprob, lnlike in tqdm(sampler.sample(p, lnprob0 = lnprob, iterations = config.nsteps),total=config.nsteps):
                percentiles.append(np.percentile(p,pvalues,axis=0))
                count += 1
                if count % config.nstepeval:
                    continue
                tau = [autocorr_new(sampler.chain[:,:count,j]) for j in range(ndims_total)]
                steps.append(count)
                autocorr_time.append(np.mean(tau))
                if config.test_conv:
                    converged = (np.mean(tau) * 100 < count)
                    if (converged) & (count > config.nstepsmin):
                        break
                else:
                    converged = None
        else:
            for sample in sampler.sample(p, iterations = config.nsteps, progress=not(config.batch)):
                p = sample.coords
                percentiles.append(np.percentile(p,pvalues,axis=0))
                count += 1
                if count % config.nstepeval:
                    continue
                tau = [autocorr_new(sampler.chain[:,:count,j]) for j in range(ndims_total)]
                steps.append(count)
                autocorr_time.append(np.mean(tau))
                if config.test_conv:
                    converged = (np.mean(tau) * 100 < count)
                    if (converged) & (count > config.nstepsmin):
                        break
                else:
                    converged = None

    # Print acceptance fraction
    print("Finished MCMC")
    print("Mean acceptance fraction: {0:.5f}".format(np.mean(sampler.acceptance_fraction)))
    if config.with_autocorr:
        np.savetxt(os.path.join(output_folder,'autocorrtime_evolution.csv'), np.asarray([steps,autocorr_time]).T,
            delimiter=",", header="Nsteps,Integrated_autocorr_time")

    return sampler, np.array(percentiles)

def autocorr_new(y, c=5.0):
    """
    Routine which computes the autocorrelation time as explained by https://dfm.io/posts/autocorr/ 
    which solves the problem of the typical raised error in emcee.autocorr.integrated_time "AutocorrError"

    :param y: time series
    :type y: np.array

    :param c: step size of the window search
    :type c: float

    :return: the estimated autocorrelation time
    :rtype: float or np.array
   
    """
    f = np.zeros(y.shape[1])
    for yy in y:
        f += autocorr_func_1d(yy)
    f /= len(y)
    taus = 2.0 * np.cumsum(f) - 1.0
    window = auto_window(taus, c)
    return taus[window]

def autocorr_func_1d(x, norm=True):
    """
    Estimation of the normalised autocorrelation function using a fast Fourier transform (FFT)

    :param x: time series
    :type x: np.array

    :return: the estimated autocorrelation function
    :rtype: float or np.array
   
    """
    x = np.atleast_1d(x)
    if len(x.shape) != 1:
        raise ValueError("invalid dimensions for 1D autocorrelation function")
    n = next_pow_two(len(x))

    # Compute the FFT and then (from that) the auto-correlation function
    f = np.fft.fft(x - np.mean(x), n=2 * n)
    acf = np.fft.ifft(f * np.conjugate(f))[: len(x)].real
    acf /= 4 * n

    # Optionally normalize
    if norm:
        acf /= acf[0]

    return acf

def next_pow_two(n):
    """
    Finds the next power of two, useful to compute the FFT

    :type n: integer
    """
    i = 1
    while i < n:
        i = i << 1
    return i

def auto_window(taus, c):
    """
    Computes automatically the window procedure following Sokal (1989)

    :param taus: values of the autocorrelation function
    :type taus: float or np.array
    """
    m = np.arange(len(taus)) < c * taus
    if np.any(m):
        return np.argmin(m)
    return len(taus) - 1

def dont_run_emcee(p0):
    """
    Dummy subroutine which simply returns the initial distribution without doing any emcee
    iterations.

    :param p0: the initial set of walkers
    :type p0: np.array

    :return: the dummy sampler for the MCMC run, dummy array with walker percentiles
    :rtype: dummy sampler object, np.array
    """

    config.nsteps = 1
    if (config.PT):
        samples = p0.reshape([config.ntemps, config.nwalkers, 1, ndims_total])
        lnprob = np.empty([config.ntemps, config.nwalkers])
        for i in range(config.ntemps):
            for j in range(config.nwalkers):
                lnprob[i,j] = prob(p0[i,j,:])
    else:
        samples = p0.reshape([config.nwalkers, 1, ndims_total])
        lnprob = np.empty([config.nwalkers])
        for j in range(config.nwalkers):
                lnprob[j] = prob(p0[j,:])

    class dummy_sampler:
        def __init__(self, _samples, _lnprob):
            self.chain          = _samples
            self.lnprobability  = _lnprob

    # fake percentiles
    percentiles = np.empty((2,ndims_total),dtype=model.gtype)
    percentiles[:,:] = np.nan
    return dummy_sampler(samples, lnprob), percentiles

def find_blobs(samples):
    """
    Find blobs (i.e. supplementary output parameters) from a set of samples
    (i.e. for multiple models).
    
    :param samples: input set of samples
    :type samples: list/array of array-like

    :return: set of supplementary output parameters
    :rtype: np.array
    """

    blobs = my_map(find_a_blob,samples)
    blobs = np.asarray(blobs)
    n = len(output_params_index)
    for i in range(n):
        for j in range(nstars):
            k = i + j*n
            blobs[:,k] =  model.apply_functions(output_params_funcs[i],blobs[:,k])
    return blobs

def find_a_blob(params):
    """
    Find a blob (i.e. supplementary output parameters) for a given set of parameters
    (for one model).  The blob also includes the log(P) value as a first entry.
    
    :param params: input set of parameters
    :type params: array-like

    :return: list of supplementary output parameters
    :rtype: list of floats
    """

    n = len(output_params_index)
    result = np.empty((nstars*n,),dtype=model.gtype)
    for i in range(nstars):
        my_model = model.interpolate_model(grid,params[prob.param_map[i]],grid.tessellation,grid.ndx)
        result[i*n:(i+1)*n] = my_model[output_params_index]
    return result

def find_output_params(my_model):
    """
    This finds the supplementary output params as specified
    by the config.output_params variable.

    :param my_model: a model for which we wish to obtain the
                     supplementary output parameters
    :type my_model: np.array

    :return: a list with the supplementary parameters
    :rtype: list of floats
    """

    return [model.apply_functions(output_params_funcs[i], \
            my_model[output_params_index[i]])         \
            for i in range(len(output_params_index))]

def write_samples(filename, labels, samples):
    """
    Write raw samples to a file.
        
    :param filename: name of file in which to write the samples
    :param labels:   names of relevant variables (used to write a header)
    :param samples:  samples for which statistical properties are calculated

    :type filename: string
    :type labels:   list of strings
    :type samples:  array-like
    """
    
    # write results to file
    (m,n) = np.shape(samples)
    output_file = open(filename,"w")
    output_file.write("#")
    for label in labels: output_file.write(' {0:22}'.format(label))
    output_file.write("\n ")
    for i in range(m):
        for j in range(n):
            output_file.write(' {0:22.15e}'.format(samples[i,j]))
        output_file.write("\n ")
    output_file.close()

def write_statistics(filename, labels, samples):
    """
    Write statistical properties based on a sequence of realisations to a file.
    The results include:
    
    - average values for each variable (statistical mean)
    - error bars for each variable (standard mean deviation)
    - correlation matrix between the different variables
    
    :param filename: name of file in which to write the statistical properties
    :param labels:   names of relevant variables
    :param samples:  samples for which statistical properties are calculated

    :type filename: string
    :type labels:   list of strings
    :type samples:  np.array
    """

    # initialisation
    (m,n) = np.shape(samples)
    average = samples.sum(axis=0, dtype=np.float64)/(1.0*m)
    covariance = np.zeros((n,n), dtype=np.float64)

    # find covariance matrix:
    for i in range(n):
        for j in range(i+1):
            covariance[i,j] = np.dot(samples[:,i]-average[i],samples[:,j]-average[j])/(1.0*m)
            covariance[j,i] = covariance[i,j]

    # write results to file
    output_file = open(filename,"w")
    output_file.write("Summary\n=======\n");
    for i in range(n):
        output_file.write('{0:25} {1:25.15e} {2:25.15e}\n'.format(labels[i], \
                          average[i],math.sqrt(covariance[i,i])))
    output_file.write("\nCorrelation matrix\n==================\n");

    output_file.write('{0:25} '.format(" "))
    for i in range(n):
        output_file.write('{0:25} '.format(labels[i]))
    output_file.write("\n")

    for i in range(n):
        output_file.write('{0:25} '.format(labels[i]))
        for j in range(n):
            output_file.write('{0:25.15e} '.format(covariance[i,j] \
                        /math.sqrt(covariance[i,i]*covariance[j,j])))
        output_file.write("\n")
    output_file.close()

def write_percentiles(filename, labels, samples):
    """
    Write percentiles based on a sequence of realisations to a file.
    The results include:
    
      +/- 2 sigma error bars (using the 2.5th and 97.5th percentiles)
      +/- 1 sigma error bars (using the 16th and 84th percentiles)
      the median value (i.e. the 50th percentile)
    
    :param filename: name of file in which to write the percentiles
    :param labels:   names of relevant variables
    :param samples:  samples for which statistical properties are calculated

    :type filename: string
    :type labels:   list of strings
    :type samples:  np.array
    """

    # n sigma values (from https://en.wikipedia.org/wiki/Normal_distribution)
    one_sigma = 0.682689492137
    two_sigma = 0.954499736104
    pvalues = [100.0*(0.5-two_sigma/2.0), 100.0*(0.5-one_sigma/2.0), 50.0, \
               100.0*(0.5+one_sigma/2.0), 100.0*(0.5+two_sigma/2.0)]
    #three_sigma = 0.997300203937

    # initialisation
    (m,n) = np.shape(samples)
    percentiles = np.empty((n,5), dtype=np.float64)
    for i in range(n):
        percentiles[i,:] = np.percentile(samples[:,i],pvalues)

    # write results to file
    output_file = open(filename,"w")
    output_file.write("Percentiles\n===========\n\n");
    output_file.write('{0:25} '.format("Quantity"))
    output_file.write('{0:25} '.format("-2sigma"))
    output_file.write('{0:25} '.format("-1sigma"))
    output_file.write('{0:25} '.format("Median"))
    output_file.write('{0:25} '.format("+1sigma"))
    output_file.write('{0:25}\n'.format("+2sigma"))

    for i in range(n):
        output_file.write('{0:25} '.format(labels[i]))
        for j in range(5):
            output_file.write('{0:25.15e} '.format(percentiles[i,j]))
        output_file.write('\n')
    output_file.close()

def write_readme(filename, elapsed_time):
    """
    Write parameters relevant to this MCMC run.

    :param filename: name of file in which to write the statistical properties
    :type filename: string

    :param elapsed_time: time that elapsed during the MCMC run
    :type elapsed_time: float
    """
    
    # various format related strings:
    boolean2str = ("False","True")
    str_decimal = "{0:40}{1:d}\n"
    str_string  = "{0:40}{1:40}\n"
    str_float   = "{0:40}{1:22.15e}\n"
    
    global autocorr_time
    global converged
    global count

    output_file = open(filename,"w")
    
    output_file.write(string_to_title("Observational constraints"))
    output_file.write(str_decimal.format("Number of stars",nstars))
    if (nstars > 1):
        for j in range(nstars):
            output_file.write("\nStar %s:\n"%(star_names[j]))
            for (name,param,func,distrib) in prob.likelihoods[j].constraints:
                output_file.write(str_string.format(name,distrib.to_string()))
        output_file.write("\n")
        output_file.write(str_string.format("Common parameters",str(config.common_params)))
    else:
        for (name,param,func,distrib) in prob.likelihoods[0].constraints:
            output_file.write(str_string.format(name,distrib.to_string()))

    output_file.write(string_to_title("Priors"))
    for name,prior in zip(grid_params_MCMC, prob.priors.priors):
        output_file.write(str_string.format("Prior on "+prior[0],prior[4].to_string()))
        output_file.write(str_float.format("Coef. on "+prior[0],prior[5]))

    output_file.write(string_to_title("The grid and interpolation"))
    output_file.write(str_string.format("Binary grid",config.binary_grid))
    output_file.write(str_string.format("Grid parameters",str(grid.grid_params)))
    output_file.write(str_string.format("All parameters",str(grid.all_params)))
    output_file.write(str_string.format("Distort grid prior to tessellation",boolean2str[config.distort_grid]))
    if (config.distort_grid):
        output_file.write("Distortion matrix:")
        output_file.write(str(grid.distort_mat))

    output_file.write(string_to_title("EMCEE parameters"))
    output_file.write(str_string.format("EMCEE version",emcee.__version__))
    output_file.write(str_string.format("With parallel tempering",boolean2str[config.PT]))
    if (config.PT):
        output_file.write(str_string.format("PTEMCEE version",ptemcee.__version__))
        output_file.write(str_string.format("Adaptive tempering",boolean2str[config.PTadapt]))
        output_file.write(str_decimal.format("Number of temperatures",config.ntemps))
    output_file.write(str_decimal.format("Number of walkers",config.nwalkers))
    output_file.write(str_decimal.format("Number of burn-in steps",config.nsteps0))
    output_file.write(str_decimal.format("Number of production steps",config.nsteps))
    output_file.write(str_decimal.format("Thinning parameter",config.thin))

    output_file.write(string_to_title("Initialisation"))
    if (config.samples_file is None):
        output_file.write(str_string.format("Initialise with a tight ball",boolean2str[config.tight_ball]))
        output_file.write(str_string.format("Tight ball behaviour",config.tight_ball_behaviour))
        if (config.tight_ball):
            if (nstars > 1):
                for j in range(nstars):
                    output_file.write("\nStar %s:\n"%(star_names[j]))
                    for (name,distrib) in zip(grid_params_MCMC,initial_distributions[j].priors):
                        output_file.write(str_string.format("Tight ball range on "+distrib[0], \
                            distrib[4].to_string()))
            else:
                for (name,distrib) in zip(grid_params_MCMC,initial_distributions[0].priors):
                    output_file.write(str_string.format("Tight ball range on "+distrib[0], \
                        distrib[4].to_string()))
    else:
        output_file.write(str_string.format("Initialised with samples from",config.samples_file))

    output_file.write(string_to_title("MCMC convergence estimation"))
    output_file.write(str_string.format("Iterations until convergence",str(count)))
    output_file.write(str_string.format("Integrated autocorrelation time",str(autocorr_time[-1])))
    output_file.write(str_string.format("Convergence estimation",str(converged)))

    output_file.write(string_to_title("Output"))
    output_file.write(str_string.format("List of output parameters",str(config.output_params)))
    output_file.write(str_string.format("Plot walkers",boolean2str[config.with_walkers]))
    output_file.write(str_string.format("Plot histograms",boolean2str[config.with_histograms]))
    output_file.write(str_string.format("Plot triangle plots",boolean2str[config.with_triangles]))
    output_file.write(str_string.format("List of plot extensions",str(config.plot_extensions)))
    output_file.write(str_string.format("List of triangle plot extensions",str(config.tri_extensions)))

    if (len(naccepted_parameters) > 0):
        output_file.write(string_to_title("Number of accepted/rejected models"))
        for i in range(nstars):
            output_file.write(str_decimal.format("Accepted(%s)"%(star_names[i]),naccepted_parameters[i]))
            output_file.write(str_decimal.format("Rejected(%s)"%(star_names[i]),nrejected_parameters[i]))

    output_file.write(string_to_title("Miscellaneous"))
    output_file.write(str_string.format("With parallelisation",boolean2str[config.parallel]))
    if (config.parallel):
        output_file.write(str_decimal.format("Number of processes",config.nprocesses))
    output_file.write(str_float.format("Execution time (s)",elapsed_time))

    output_file.close()

def write_model(my_model,my_params,my_result,overall_result,model_name):
    """
    Write text file with caracteristics of input model.
    
    :param my_model: model for which we're writing a text file
    :param my_params: parameters of the model
    :param my_result: ln(P) value obtained for the model
    :param overall_result: total ln(P) value obtained for the set of models
    :param model_name: name used to describe this model.  This is also used
      when naming the text file.

    :type my_model: :py:class:`model.Model`
    :type my_params: array-like
    :type my_result: float
    :type overall_result: float
    :type model_name: string
    """

    # sanity check
    if (my_model is None): return

    # initialisations:
    str_float   = "{0:40}{1:22.15e}\n"
    param_names = grid_params_MCMC + config.output_params

    output_file = open(os.path.join(output_folder,model_name+"_model.txt"),"w")
    output_file.write(string_to_title("Model: "+model_name))
    if (not np.isnan(overall_result)):
        output_file.write(str_float.format("ln(P) [all]",overall_result))
    output_file.write(str_float.format("ln(P) [this model]",my_result))
    for name,value in zip(param_names,my_params):
        output_file.write(str_float.format(name,value))

    output_file.close()

def string_to_title(string):
    """
    Create fancy title from string.
    
    :param string: string from which the title is created.
    :type string: string

    :return: the fancy string title
    :rtype: string
    """

    n = len(string)
    ntitle = 80
    nhalf  = (ntitle - n - 6)//2
    result = "\n"+"#"*ntitle + "\n"
    result += "#"*(ntitle - nhalf - n - 6)
    result += "   "+string+"   "+"#"*nhalf+"\n"
    result += "#"*ntitle + "\n"
    return result

def swap_dimensions(array):
    """
    Swaps the two first dimensions of an array.
    This is useful for handling the different conventions used to store
    the samples in emcee3 and ptemcee.

    :param array: input array
    :type array: np.array

    :return: array with swapped dimensions
    :rtype: np.array
    """

    shape = array.shape
    new_shape = (shape[1], shape[0])+shape[2:]
    new_array = np.empty(new_shape,dtype=array.dtype)
    for i in range(new_shape[0]):
        new_array[i,:,...] = array[:,i,...]
    return new_array

def plot_walkers(samples, labels, filename, nw=3):
    """
    Plot individual walkers.
    
    :param samples: samples from the emcee run
    :param labels: labels for the different dimensions in parameters space
    :param filename: specify name of file in which to save plots of walkers.
    :param nw: number of walkers to be plotted

    :type samples: np.array
    :type labels: list of strings
    :type filename: string
    :type nw: int

    .. warning::    
      This method must be applied before the samples are reshaped,
      and information on individual walkers lost.
    """
    
    plt.figure()
    itr = np.arange(count)
    for i in range(nw):
        for j in range(ndims_total):
            plt.subplot(ndims_total,nw,1+i+j*nw)
            plt.title(labels[j])
            plt.plot(itr,samples[i,:,j],'-')
    for ext in config.plot_extensions:
        plt.savefig(filename+ext)

    plt.clf()

def plot_distrib_iter(percentiles, labels, names, folder):
    """
    Plot individual distribution of walkers as a function of iterations.
    
    :param percentiles: array with percentiles from emcee run
    :param labels: labels for the different variables in parameters space
    :param names:  names of the different variables in parameters space (used for filenames)
    :param folder: specify name of file in which to save plots of walkers.

    :type percentiles: np.array
    :type labels: list of strings
    :type names: list of strings
    :type folder: string

    .. warning::    
      This method must be applied before the samples are reshaped,
      and information on individual walkers lost.
    """

    nsteps = config.nsteps0 + count  # total number of steps
    yfill  = np.empty((2*nsteps,),dtype=np.float64)
    xfill  = np.array(list(range(nsteps))+list(range(nsteps-1,-1,-1)))
    for i in range(ndims_total):
        plt.figure()
        yfill[:nsteps] = percentiles[:,0,i]    # 25th percentile
        yfill[nsteps:] = percentiles[::-1,2,i] # 75th percentile
        plt.fill(xfill,yfill,"c")
        plt.plot(xfill[:nsteps],percentiles[:,1,i],"b") # 50th percentile
        plt.axvline(config.nsteps0,ls=":",c="k")
        plt.title(labels[i])
        plt.xlabel(r"Iteration, $n$")
        plt.ylabel(r"Walker distribution")
        for ext in config.plot_extensions:
            plt.savefig(os.path.join(output_folder,"distrib_iter_"+names[i]+"."+ext))
        plt.clf()

def find_weights_hist(array, nbins):
    """
    This finds appropriate weights for created a normalised histogram (thus avoiding
    the use of the keywords "normed" or "density").

    :param array: the array to be normalised
    :type array: numpy float array

    :param nbins: number of bins involved in the histogram
    :type nbins: int

    :return: the weight array
    :rtype: numpy float array
    """

    array_range = np.max(array)-np.min(array)
    return np.ones_like(array)*float(nbins)/(float(len(array))*array_range)

def plot_histograms(samples, names, fancy_names, truths=None):
    """
    Plot a histogram based on a set of samples.
    
    :param samples: samples form the emcee run
    :param names: names of the quantities represented by the samples.  This will
      be used when naming the file with the histogram
    :param fancy_names: name of the quantities represented by the samples. This
      will be used as the x-axis label in the histogram.
    :param truths: reference values (typically the true values or some other
      important values) to be added to the histograms as a vertical line

    :type samples: np.array
    :type names: list of strings
    :type fancy_names: list of strings
    :type truths: list of floats
    """

    for i in range(len(names)):
        plt.figure()

        # this works with older versions of matplotlib, but is deprecated in newer versions:
        #n, bins, patches = plt.hist(samples[:,i],50,normed=True,histtype='bar') 

        # this works with newer versions of matplotlib, but not older versions:
        #n, bins, patches = plt.hist(samples[:,i],50,density=True,histtype='bar') 

        # this should hopefully work in old and new versions of matplotlib:
        nbins = 50
        n, bins, patches = plt.hist(samples[:,i],bins=nbins, \
            weights=find_weights_hist(samples[:,i],nbins),histtype='bar') 

        if (truths is not None):
            if (i < len(truths)):
                xlim = plt.xlim()
                ylim = plt.ylim()
                if (abs((truths[i]-(xlim[0]+xlim[1])/2.0)/(xlim[1]-xlim[0])) <= 1.5):
                    plt.plot([truths[i],truths[i]],ylim,'g-')
                    plt.ylim(ylim) # restore limits, just in case
        plt.xlabel(fancy_names[i])
        for ext in config.plot_extensions:
            plt.savefig(os.path.join(output_folder,"histogram_"+names[i]+"."+ext))

        plt.clf()
        plt.close()

def interpolation_tests(filename):
    """
    Carry out various interpolation tests and write results in
    binary format to file.

    :param filename: name of file in which to write test results.
    :type filename: string

    .. note::
      The contents of this file may be plotted using methods from
      ``plot_interpolation_test.py``.
    """

    grid = load_binary_data(config.binary_grid)
    grid.set_age_indices()
    grid.prepare_interpolation()
    titles = utilities.my_map(grid.string_to_latex,grid.grid_params+[model.age_adim_str,])
    titles_all = grid.all_params_latex
    results_age1 = [track.test_interpolation(1) for track in grid.tracks]
    results_age2 = [track.test_interpolation(2) for track in grid.tracks]
    results_track, ndx1, ndx2, tessellation = grid.test_interpolation()
    output = open(filename,"wb")
    dill.dump([grid.ndim+1, model.nglb, titles, titles_all, grid.grid, ndx1, ndx2,
               tessellation, results_age1, results_age2, results_track],output)
    output.close()

if __name__ == "__main__":
    """
    SPInS = Stellar Parameters INferred Systematically
    """

    # this is for writing binary data
    if (config.mode == "write_grid"):
        write_binary_data(config.list_grid,config.binary_grid)
        sys.exit(0)

    # this is for testing the interpolation
    if (config.mode == "test_interpolation"):
        interpolation_tests(config.interpolation_file)
        sys.exit(0)

    # this is for writing isochrones
    if (config.mode == "write_isochrone"):
        grid = load_binary_data(config.binary_grid)
        grid.set_age_indices()
        grid.prepare_interpolation()
        isochrone = grid.find_isochrone(config.isochrone_parameters, config.isochrone_npts)
        if (isochrone is not None):
            write_samples(config.isochrone_file,grid.all_params,isochrone)
        sys.exit(0)

    #sanity check
    if (config.mode != "fit_data"):
        print("ERROR: unrecognised value (\"%s\") for the"%(config.mode))
        print("       variable mode in SPInS_configure.py. Aborting")
        sys.exit(1)

    # check number of arguments
    assert (len(sys.argv) > 1), "Usage: SPInS.py observations_file1 [observations_file2 ...]"

    # check parameters in SPInS_configure.py
    config.common_params = utilities.my_map(model.extract_fundamental_parameter,config.common_params)
    check_configuration()

    print('\n'+'*'*52)
    print("SPInS = Stellar Parameters INferred Systematically")
    print('*'*52)
    t0 = time.time()

    # create output folder:
    if (len(sys.argv[1:]) == 1):
        output_folder = os.path.join(config.output_dir,os.path.basename(sys.argv[1]))
    else:
        output_folder = os.path.join(config.output_dir,"combo%d_%s_%s"%( \
            len(sys.argv[1:]),os.path.basename(sys.argv[1]),os.path.basename(sys.argv[-1])))

    # check for existence of previous results:
    if (os.path.exists(output_folder+'/README')) & config.rm_existing:
        # if there are results but we want to remove them
        print('Already results for star: ',sys.argv[1:],', removing')
        shutil.rmtree(output_folder)
        os.makedirs(output_folder)
    elif  (os.path.exists(output_folder+'/README')) & (not config.rm_existing):
        #if there are some results we preserve them and exit the code
        print('Already results for star: ',sys.argv[1:],', exiting')
        sys.exit(0)
    elif (not os.path.exists(output_folder+'/README')):
        # sometimes the folder is created but no results, we remove and run the code
        if (os.path.exists(output_folder)):
            shutil.rmtree(output_folder)
        os.makedirs(output_folder)

    # seed random number generator (NOTE: this is not thread-safe):
    np.random.seed()

    # load grid and associated quantities
    grid = load_binary_data(config.binary_grid)
    grid.set_age_indices()
    grid.prepare_interpolation()
    grid_params_MCMC = grid.grid_params + [model.age_str,]
    grid_params_MCMC_index = utilities.my_map(grid.string_to_index,grid_params_MCMC)
    grid_params_MCMC_fundamental = utilities.my_map(model.extract_fundamental_parameter,grid_params_MCMC)
    nstars           = len(sys.argv)-1
    ncommon          = len(config.common_params)
    ndims            = len(grid_params_MCMC)
    ndims_total      = (ndims-ncommon)*nstars + ncommon
    star_names       = sys.argv[1:]

    # sanity check
    for elt in config.output_params:
        assert (model.extract_fundamental_parameter(elt) in grid.all_params_fundamental), \
               "%s not present in grid.  Please modify SPInS_configure.py"%(elt)
    for elt in config.common_params:
        assert(elt in grid_params_MCMC_fundamental), \
               "%s cannot be a common parameter.  Please modify SPInS_configure.py"%(elt)

    # define likelihood functions (1 per star):
    likes = []
    for i in range(nstars):
        like = Likelihood()
        like.read_constraints(star_names[i])
        #like.add_constraints_from_prior(config.priors) # this should be deprecated
        likes.append(like)
    star_names = utilities.my_map(os.path.basename,star_names)

    # initialise other variables
    output_params_index, output_params_funcs = zip(*utilities.my_map(grid.string_to_param,config.output_params))
    output_params_index = np.asarray(output_params_index)

    # define priors:
    priors = Prior_list(config.priors)
    if (config.tight_ball):
        for i in range(ndims):
            if (priors.priors[i] is None):
                priors.priors[i] = (grid_params_MCMC[i],[],[],[],Distribution("Uninformative",[]),1.0)
    else:
        for i in range(ndims):
            if (priors.priors[i] is None):
                priors.priors[i] = (grid_params_MCMC[i],[],[],[],Distribution("Uniform",grid.range(grid_params_MCMC[i])),1.0)

    # combine the above:
    prob = Probability(priors,likes)

    # titles, labels, etc.
    # labels       = all labels without star names (variables are NOT repeated for
    #                different stars)
    # labels_stars = all labels including star names when needed
    # names_stars  = all names including star names when needed
    labels = ["ln(P)",]+utilities.my_map(grid.string_to_latex,grid_params_MCMC) \
           +  utilities.my_map(grid.string_to_latex,config.output_params)
    if (nstars == 1):
        labels_stars = labels
        names_stars  = ["lnP",]+grid_params_MCMC+config.output_params
    else:
        labels_stars = ["ln(P)",]
        names_stars = ["lnP",]
        for i in range(nstars):
            for j in range(ndims):
                if (grid_params_MCMC_fundamental[j] in config.common_params):
                    if (i == 0): 
                        labels_stars.append(grid.string_to_latex(grid_params_MCMC[j]))
                        names_stars.append(grid_params_MCMC[j])
                else:
                    labels_stars.append(grid.string_to_latex(grid_params_MCMC[j]) \
                         +" [%s]"%(star_names[i]))
                    names_stars.append(grid_params_MCMC[j]+"_"+star_names[i])
        for i in range(nstars):
            f1 = lambda st: grid.string_to_latex(st)+" [%s]"%(star_names[i])
            f2 = lambda st: st +"_"+star_names[i]
            labels_stars += utilities.my_map(f1,config.output_params)
            names_stars  += utilities.my_map(f2,config.output_params)

    # start pool of parallel processes
    # NOTE: multiprocessing.pool.TheadPool doesn't duplicate memory
    #       like Pool, but can actually slow down execution (even
    #       compared to non-parallel execution).
    if (config.parallel):
        set_start_method("fork")
        pool = Pool(processes = config.nprocesses)
        my_map = pool.map
    else:
        pool = None
        my_map = utilities.my_map

    # initialised walkers:
    p0 = init_walkers()

    # run emcee:
    sampler, percentiles = run_emcee(p0)

    # Collect results:
    if (config.PT):
        samples = sampler.chain[0,:,:count,:]
        lnprob  = sampler.logprobability[0,:,:count]
    else:
        if (mc2):
            samples = sampler.chain[:,:count,:]
            lnprob  = sampler.lnprobability[:,:count]
        else:
            samples = swap_dimensions(sampler.get_chain())[:,:count,:]
            lnprob  = swap_dimensions(sampler.get_log_prob())[:,:count]
    if (config.with_chain):
        np.save(os.path.join(output_folder,"mcmc_chain.npy"),samples)

    # Write file with parameters used in this run
    elapsed_time = time.time() - t0
    write_readme(os.path.join(output_folder,"README"),elapsed_time)

    # Various diagnostic which must be done before reshaping the samples:
    if (config.with_walkers):
        plot_walkers(samples, labels_stars[1:ndims_total+1], os.path.join(output_folder,"walkers."), nw = 3)

    if (config.with_distrib_iter):
        plot_distrib_iter(percentiles, labels_stars[1:ndims_total+1], \
                          names_stars[1:ndims_total+1], output_folder)

    # Reshape the samples and obtain auxiliary quantities:
    # NOTE: choosing order='C' leads to much better sub-sampling since the
    #       the thin_samples array is much more likely to draw from all of the
    #       walkers rather than a subset. Otherwise, if order='F' and if
    #       nwalkers is a multiple of thin, than only 1 out thin walkers
    #       is used (and this leads to poor sampling, repititions, etc.).
    samples = samples.reshape((config.nwalkers*count,ndims_total),order='C')
    lnprob  = lnprob.reshape((config.nwalkers*count,1),order='C')
    samples = np.concatenate((lnprob,samples),axis=1)
    
    thin_samples = samples[0::config.thin,:]
    blobs = find_blobs(thin_samples[:,1:])
    samples_big = np.concatenate((thin_samples,blobs),axis=1)

    # end pool of parallel processes
    if (config.parallel):
        pool.close()
        pool.join()

    # write various text files:
    if (config.with_samples):
        write_samples   (os.path.join(output_folder,"samples.txt"),    names_stars[:ndims_total+1],samples)
        if config.with_big:
            write_samples   (os.path.join(output_folder,"samples_big.txt"),names_stars,     samples_big)
    if (config.with_results):
        write_statistics(os.path.join(output_folder,"results.txt"),    names_stars[1:], samples[:,1:])
        write_percentiles(os.path.join(output_folder,"percentiles.txt"),     \
                          names_stars[1:], samples[:,1:])
        if config.with_big:
            write_statistics(os.path.join(output_folder,"results_big.txt"),names_stars[1:], samples_big[:,1:])
            write_percentiles(os.path.join(output_folder,"percentiles_big.txt"), \
                          names_stars[1:], samples_big[:,1:])

    if (config.with_models):
        # find best MCMC model
        ndx_max = lnprob.argmax()
        best_MCMC_result = lnprob[ndx_max,0]
        best_MCMC_params = samples[ndx_max,1:].reshape((ndims_total))
        for i in range(nstars):
            best_MCMC_model = model.interpolate_model(grid,best_MCMC_params[prob.param_map[i]],grid.tessellation,grid.ndx)
            local_result = prob.evaluate(best_MCMC_model,i)
            prefix = star_names[i]+"_"
            if (nstars == 1): prefix = "" 
            write_model(best_MCMC_model,list(best_MCMC_params[prob.param_map[i]]) \
                +find_output_params(best_MCMC_model),local_result,best_MCMC_result,prefix+"best_MCMC")
    
        # find statistical model
        statistical_params = samples[:,1:].sum(axis=0, dtype=np.float64)/float(config.nwalkers*config.nsteps)
        statistical_params.reshape(ndims_total)
        statistical_result = prob(statistical_params)
        for i in range(nstars):
            statistical_model = model.interpolate_model(grid,statistical_params[prob.param_map[i]],grid.tessellation,grid.ndx)
            if (statistical_model is not None):
                local_result = prob.evaluate(statistical_model,i)
                prefix = star_names[i]+"_"
                if (nstars == 1): prefix = "" 
                write_model(statistical_model,list(statistical_params[prob.param_map[i]]) \
                    +find_output_params(statistical_model),local_result,statistical_result,prefix+"statistical")

    # make various plots:

    if (config.with_histograms):
        plot_histograms(samples_big[:,1:],names_stars[1:],labels_stars[1:],truths=best_grid_params)
        if (config.tight_ball):
            plot_histograms(samples[:,0:1],["lnP"],["ln(P)"],truths=[best_grid_result])
        else:
            plot_histograms(samples[:,0:1],["lnP"],["ln(P)"])

    if (config.with_triangles):
        if (nstars > 1):
            n_output = len(config.output_params)
            indices = np.empty((ndims+n_output,),dtype=int)
            for i in range(nstars):
                if (best_grid_params is not None):
                    fig = corner.corner(samples[:,1+prob.param_map[i]], \
                        truths=best_grid_params[prob.param_map[i]], labels=labels[1:ndims+1])
                else:
                    fig = corner.corner(samples[:,1+prob.param_map[i]], labels=labels[1:ndims+1])
                for ext in config.tri_extensions:
                    fig.savefig(os.path.join(output_folder,star_names[i]+"_triangle."+ext))
                plt.close('all')
                if (config.with_big):
                    indices[:ndims] = 1+prob.param_map[i]
                    indices[ndims:] = np.arange(n_output) + i*n_output + ndims_total + 1
                    fig = corner.corner(samples_big[:,indices], labels=labels[1:])
                    for ext in config.tri_extensions:
                        fig.savefig(os.path.join(output_folder,star_names[i]+"_triangle_big."+ext))
                    plt.close('all')

        if (best_grid_params is not None):
            fig = corner.corner(samples[:,1:], truths=best_grid_params, labels=labels_stars[1:ndims_total+1])
        else:
            fig = corner.corner(samples[:,1:], labels=labels_stars[1:ndims_total+1])
        for ext in config.tri_extensions:
            fig.savefig(os.path.join(output_folder,"triangle."+ext))
        plt.close('all')

        if (config.with_big):
            fig = corner.corner(samples_big[:,1:], labels=labels_stars[1:])
            for ext in config.tri_extensions:
                fig.savefig(os.path.join(output_folder,"triangle_big."+ext))
            plt.close('all')

    print('\nOutputs saved, exiting')
    print('*'*52)
