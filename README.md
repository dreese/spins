Stellar Parameters INferred Systematically (SPInS)
==================================================

Description
-----------

**History**:
  * SPInS is a spin-off of the [AIMS code](https://lesia.obspm.fr/perso/daniel-reese/spaceinn/aims/)
    [(Rendle et al. 2019)](https://ui.adsabs.harvard.edu/abs/2019MNRAS.484..771R/abstract)
    in which the seismic part has been removed (thus reducing the computational cost)

**Goals**:
  * estimate stellar parameters and credible intervals/error bars
  * chose a representative set or sample of reference models

**Inputs**:
  * classic constraints (Teff, L, ...) and associated probability distributions

**Requirements**:
  * a *precalculated* grid of models including global parameters such as M, R, Teff, age, ...

**Methodology**:
  * applies an MCMC algorithm based on the python package [emcee](https://emcee.readthedocs.io/en/stable/)
    Relevant articles include:
    - [Bazot et al. (2012, MNRAS 427, 1847)](https://ui.adsabs.harvard.edu/abs/2012MNRAS.427.1847B/abstract)
    - [Gruberbauer et al. (2012, ApJ 749, 109)](https://ui.adsabs.harvard.edu/abs/2012ApJ...749..109G/abstract)
  * interpolates within the grid of models using Delaunay tessellation
    (from the [scipy.spatial](http://docs.scipy.org/doc/scipy/reference/spatial.html)
    package which is based on the [Qhull](http://www.qhull.org/) library)
  * modular approach: facilitates including contributions from different
    people

Authors
-------

  * Daniel R. Reese
  * Yveline Lebreton

Acknowledgements
----------------

  * If SPInS is used in any publication, the authors kindly ask you to cite the article:
    - [Lebreton & Reese, 2020, *"SPInS, a pipeline for massive stellar parameter inference"*, A&A 642, A88](https://ui.adsabs.harvard.edu/abs/2020A%26A...642A..88L/abstract)

Supplementary documentation
---------------------------

  * a computer-generated documentation is available [here](https://dreese.pages.obspm.fr/spins/)
  * this documentation includes links to [BaSTI](http://basti.oa-teramo.inaf.it/index.html) grids in SPInS format
  * a PDF version of the computer-generated documentation may be downloaded
    [here](https://gitlab.obspm.fr/dreese/spins/-/blob/master/doc/files/SPInS.pdf)
  * to generate the above documentation, install [sphinx](https://www.sphinx-doc.org/en/master/)
    and [sphinx-rtd-theme](https://github.com/readthedocs/sphinx_rtd_theme), then
    go into the `doc/` folder and type `make html`.  Finally,
    open the file `_build/html/index.html` in a web browser
  * a more technical overview of SPInS is available
    [here](https://gitlab.obspm.fr/dreese/spins/-/blob/master/doc/files/Overview.pdf)

Downloading SPInS
-----------------

  * use the following command to clone SPInS:
       `git clone git@gitlab.obspm.fr:dreese/spins.git`
  * alternatively, download SPInS from its gitlab [website](https://gitlab.obspm.fr/dreese/spins)

Installation
------------

  * most of SPInS is written in [python](https://www.python.org/), but a few
    computationally intensive parts are written in [fortran](https://en.wikipedia.org/wiki/Fortran)
  * to compile the fortran parts, edit the [Makefile](https://gitlab.obspm.fr/dreese/spins/-/blob/master/Makefile)
    and type `make` in a terminal

Copyright information
---------------------

  * the SPInS project is distributed under the terms of the
    [GNU General Public License, version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
  * a copy of of this license may be downloaded [here](https://gitlab.obspm.fr/dreese/spins/-/blob/master/COPYING)
    and should also be included in [SPInS.tgz](https://gitlab.obspm.fr/dreese/spins/-/blob/master/doc/files/SPInS.tgz)

