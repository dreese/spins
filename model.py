#!/usr/bin/env python
# coding: utf-8
# $Id: model.py
# Author: Daniel R. Reese <daniel.reese@obspm.fr>
# Copyright (C) Daniel R. Reese and Yveline Lebreton
# Copyright license: GNU GPL v3.0
#
#   SPInS is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with SPInS.  If not, see <http://www.gnu.org/licenses/>.
#

"""
A module which contains various classes relevant to the grid of models:

- :py:class:`Track`: an evolutionary track
- :py:class:`Model_grid`: a grid of evolutionary tracks

These different classes allow the program to store a grid of models and perform
a number of operations, such as:

- retrieving model properties
- interpolate within the grid models
- sort the models within a given evolutionary track
- ...

"""

__docformat__ = 'restructuredtext'

# SPInS configuration:
import SPInS_configure as config

# various packages needed for SPInS
import math
import numpy as np
import random
import matplotlib
if (config.backend is not None): matplotlib.use(config.backend)
import matplotlib.pyplot as plt
from scipy.spatial import Delaunay
from bisect import bisect_right
import sys
import ast
import time

# packages from within SPInS:
import utilities
import spins_fortran

# provide a way to deactivate tqdm, if SPInS is running in batch mode
# or if tqdm is not available:
if (config.batch):
    def tqdm(x,total=None):
        return x
else:
    from tqdm import tqdm

# Global parameters:
tol     = 1e-10
""" tolerance level for slightly negative interpolation coefficients """

eps     = 1e-6
""" relative tolerance on parameters used for setting up evolutionary tracks """

gtype   = np.float64
""" type used for grid data """

nglb    = 0
""" total number of global quantities in a model (including age parameter)."""

iage    = 1
""" index for physical age (this will be reset later on) """

iage_adim = 0
""" index for dimensionless age parameter (this will be reset later on) """

age_str     = "Age"
""" Name of the physical age parameter used in the interpolation """

age_adim_str= "Age_adim"
""" Name of the non-dimensional age parameter used in the interpolation """

# parameters needed for interpolation (these will be reset later on):
nmodelsmax = 0
""" Maximum number of models in a track """

age_array = None
""" Workspace needed for interpolation which will contain physical ages """

age_adim_array = None
""" Workspace needed for interpolation which will contain non-dimensional age parameters """

indices_along_track = None
""" Workspace which will contain indices along tracks right below input age """

weights_along_track = None
""" Workspace which will contain interpolation weights along tracks """

# Various quantities which intervene when applying a function to grid data
function_left  = {"ln": r"\ln\left(", "log": r"\log_{10}\left(", "exp": r"\exp\left(",
                  "pow10": r"10^{", "sqrt": r"\sqrt{", "square": r"\left(", "inv": r"\frac{1}{"}
function_right = {"ln": r"\right)", "log": r"\right)", "exp": r"\right)", "pow10": r"}",
                  "sqrt": r"}", "square": r"\right)^2", "inv": r"}"}
function_inv   = {"ln": "exp", "log": "pow10", "exp": "ln", "pow10": "log",
                  "sqrt": "square", "square": "sqrt", "inv": "inv"}

def pow10(x):     return 10.0**x
def square(x):    return x*x
def inv(x):       return 1.0/x
def der_log(x):   return 0.43429448190325176/x
def der_pow10(x): return 2.302585092994046*10.0**x
def der_sqrt(x):  return 1.0/(2.0*np.sqrt(x))
def twice(x):     return 2.0*x
def der_inv(x):   return -1.0/(x*x)

# NOTE: ln(10) = 2.302585092994046
#     1/ln(10) = 0.43429448190325176

function_map   = {"ln": np.log, "log": np.log10, "exp": np.exp, "pow10": pow10,
                  "sqrt": np.sqrt, "square": square, "inv": inv}
function_map_der = {"ln": inv, "log": der_log, "exp": np.exp, "pow10": der_pow10,
                    "sqrt": der_sqrt, "square": twice, "inv": der_inv}

class Track:
    """
    An evolutionary track.
    """

    def __init__(self, models, params):
        """
        :param models: models to be included in the evolutionary track.
        :type models: 2D float np.array

        :param params: parameters which describe the evolutionary track.
        :type params: 1D float np.array
        """

        self.params = params
        """Parameters which characterise this evolutionary track"""

        self.models = models
        """Properties of the models in this evolutionary track"""

    def __del__(self):
        """
        Delete the present track and the arrays is contains.
        """

        del self.params
        del self.models

    def append_track(self, aTrack):
        """
        :param aTrack: an evolutionary track to be appended to (and hence merged with)
          the present track.
        :type aTrack: 2D float np.array
        """

        self.models = np.concatenate((self.models,aTrack.models),axis=0)

    def thin(self, n):
        """
        Thin out the track by only keeping every n models.
        This is used to test interpolation along a track.

        :param n: thinning parameter
        :type n: int
        """

        self.models = self.models[0::n,:]

    def sort(self):
        """Sort models within evolutionary track according to age parameter."""

        ind = np.argsort(self.models[:,iage_adim])
        self.models = self.models[ind,:]

    def is_sorted(self):
        """
        Check to see of models are in ascending order according to physical age.
        The physical age is used as it is assumed that the track is already
        sorted according to age parameter.  Hence this is a sanity check on
        the dimensionless age parameter.

        :return: ``True`` if the models ar in order of increasing age
        :rtype: boolean
        """

        return all(self.models[i,iage] <= self.models[i+1,iage] for i in range(self.models.shape[0]-1))

    def age_adim_to_scaled_param(self,iparam,forward=True):
        """
        Replace dimensionless age parameter by a scaled version of a parameter
        as given by the index iparam.

        :param iparam: index of the parameter which will replace the dimensionless age
                       parameter
        :param forward: set this to True if the parameter increases with age, and
                        to False in the opposite situation

        :type iparam: int
        :type forward: boolean
        """

        if (forward):
            param_start = np.min(self.models[:,iparam])
            param_stop  = np.max(self.models[:,iparam])
        else:
            param_start = np.max(self.models[:,iparam])
            param_stop  = np.min(self.models[:,iparam])
        self.models[:,iage_adim] = (self.models[:,iparam]-param_start)/(param_stop-param_start)

    def remove_duplicates(self):
        """
        Removes models with duplicate age parameters from the track and notifies the user.

        .. note::
            The dimensionless age is used as this is what is involved in the interpolation.

        .. warning::
            This method should only be applied after the track has been
            sorted.
        """

        duplicates = []
        for i in range(self.models.shape[0]-1):
            if self.models[i,iage_adim] == self.models[i+1,iage_adim]:
                duplicates.append(i+1)
        if (len(duplicates) > 0):
            self.models = np.delete(self.models,duplicates,axis=0)
            print("WARNING: found duplicate models in track "+str(self.params))
            print("         These models have been removed.")
        return

    def duplicate_ages(self):
        """
        Check to see if the track contains models with duplicate values for the
        age parameter.

        :return: ``True`` if there are duplicate age(s)
        :rtype: boolean

        .. note::
            The dimensionless age is used as this is what is involved in the interpolation.

        .. warning::
            This method should only be applied after the track has been
            sorted.
        """
        for i in range(self.models.shape[0]-1):
            if self.models[i,iage_adim] == self.models[i+1,iage_adim]:
                return [True,i,i+1]
        return [False,]

    def interpolate_model(self, age_adim):
        """
        Return a model at a given non-dimensional age which is obtained using linear
        interpolation.

        :param age_adim: non-dimensional age of desired model
        :type age_adim: float

        :return: the interpolated model
        :rtype: np.array

        .. warning::
          This method assumes the track is sorted, since it applies
          a dichotomy for increased efficiency.
        """
        
        # easy exit:
        if (age_adim < self.models[0,iage_adim]): return None
        if (age_adim > self.models[-1,iage_adim]): return None
        
        istart = 0
        istop  = len(self.models)-1
        while (istop > istart+1):
            itemp = (istop+istart)//2
            if (age_adim < self.models[itemp,iage_adim]):
                istop = itemp
            else:
                istart = itemp
        mu = (age_adim - self.models[istart,iage_adim]) \
           / (self.models[istop,iage_adim] - self.models[istart,iage_adim])

        return combine_models(self.models[istart,:],1.0-mu,self.models[istop,:],mu)

    def test_interpolation(self, nincr):
        """
        Test accuracy of interpolation along evolutionary track.

        This method compares each model on a track with a linear combination of the
        adjacent models nincr increments away.  Specifically, it compares the
        original and interpolated global parameters.

        :param nincr: increment with which to carry out the interpolation.
          By comparing results for different values of ``nincr``, one can
          evaluate how the interpolation error depends on the size of the
          interval over which the interpolation is carried out.
        :type nincr: int

        :return: the interpolation errors
        :rtype: np.array 

        .. note::
            The dimensionless age is used as this is what is involved in the interpolation.
        """

        # initialisation
        nmodels = self.models.shape[0]
        ndim = len(self.params)+1
        result = np.zeros((nmodels-2*nincr,ndim+nglb),dtype=gtype)

        # loop through all models:
        for i in range(nincr,nmodels-nincr):
            # carry out interpolation
            mu = (self.models[i,iage_adim]   - self.models[i-nincr,iage_adim]) \
               / (self.models[i+nincr,iage_adim] - self.models[i-nincr,iage_adim])
            aModel = combine_models(self.models[i-nincr,:],1.0-mu,self.models[i+nincr,:],mu)

            result[i-nincr,0:ndim-1] = self.params
            result[i-nincr,ndim-1] = self.models[i,iage]
            result[i-nincr,ndim:ndim+nglb] = compare_models(aModel,self.models[i,:])

        return result

class Model_grid:
    """
    A grid of models.
    """

    def __init__(self):
        self.ndim = 0
        """
        Number of dimensions for the grid (excluding age parameters), as based on the
        :py:data:`Model_grid.grid_params` variable
        """

        self.tracks = []
        """ List of evolutionary tracks contained in the grid. """

        self.ndx = []
        """ List containing track indices. """

        self.all_params = None
        """ All of the parameters of the grid (including age parameters).  """

        self.all_params_fundamental = None
        """
        All of the fundamental parameters of the grid (i.e. without application of functions).
        """

        self.all_params_latex = None
        """ Latex version of all of the parameters of the grid (including age parameters).  """

        self.grid_params = None
        """
        Set of parameters (excluding age parameters) used to construct the grid and do
        interpolations.

        .. note::        
          For best interpolation results, these parameters should be comparable.
        """

        self.grid = None
        """
        Array containing the grid parameters for each evolutionary track (excluding age parameters).
        """

        self.tessellation = None
        """ Object containing the tessellation of the grid used for interpolation. """

        self.distort_mat = None
        """
        Transformation matrix used to break the Cartesian character of the grid and reduce
        computation time
        """

    def read_model_list(self,filename):
        """
        Read list of model parameters from a file and construct a grid.

        :param filename: name of the file with the list.
        :type filename: string
        """

        # read header:
        f = open(filename,"r")
        line1 = f.readline()
        line2 = f.readline()
        line3 = f.readline()
        f.close()
        self.all_params = ast.literal_eval(line1[2:])
        self.all_params_latex = ast.literal_eval(line2[2:])
        self.grid_params = ast.literal_eval(line3[2:])

        # sanity check:
        if (len(self.all_params) != len(self.all_params_latex)):
            print("ERROR: mismatch on number of parameters and number of")
            print("       corresponding latex representations. Aborting.")
            sys.exit(1)

        # set the correct dimensions:
        self.ndim = len(self.grid_params)

        # sanity check:
        if (self.ndim < 2):
            print("ERROR: the number of grid parameters must be equal to")
            print("       or greater than 2 in order to carry out the")
            print("       tessellation.  Unable to proceed.")
            sys.exit(1)

        # find list of columns to keep and check coherence between all_params and
        # all_params_latex
        keepme = []
        for i in range(len(self.all_params)):
            if (self.all_params[i] == "Name"): continue  # remove model names if present
            lst = extract_functions(self.all_params[i])
            lst2 = extract_functions_latex(self.all_params_latex[i])
            if (lst != lst2[0]):
                print("WARNING: mismatch between name and latex version of a variable:")
                print("    variable name: %s"%(self.all_params[i]))
                print("    latex version: %s"%(self.all_params_latex[i]))
                print("    May lead to inaccurate or complicated latex name.")

            if (self.all_params[i] not in config.apply_function):
                keepme.append(i)
            else:
                function = config.apply_function[self.all_params[i]]
                if (function == "del"): 
                    # sanity check
                    if (self.all_params[i] in self.grid_params):
                        sys.exit("ERROR: removing grid variables is not allowed.  Aborting")
                else:
                    keepme.append(i)

        # remove deleted variables from self.all_params and self.all_params_latex
        self.all_params = [self.all_params[i] for i in keepme]
        self.all_params_latex = [self.all_params_latex[i] for i in keepme]
        self.all_params_fundamental = utilities.my_map(extract_fundamental_parameter,self.all_params)
        nglb = len(self.all_params)

        # read file and skip header (comments beginning with "#" are skipped by default):
        # NOTE: if no age parameter is provided, add supplementary column of zeros for it
        #       (proper values can be calculated later on, according to config.replace_age_adim)
        if (age_adim_str not in self.all_params):
            self.all_params.append(age_adim_str)
            self.all_params_latex.append(r"Age parameter, $\tau$")
            self.all_params_fundamental.append(age_adim_str)
            keepme.append(keepme[0])  # adding an extra value here allows the program to directly create
                                      # a with the correct dimensions rather than having to append a column
            a = np.loadtxt(filename,dtype=gtype,usecols=keepme)
            a[:,-1] = 0.0 # very important for avoiding errors later on
        else:
            a = np.loadtxt(filename,dtype=gtype,usecols=keepme)

        # sanity check (probably no longer needed):
        if (a.shape[1] != len(self.all_params)):
            print("ERROR: mismatch on number of parameters in header in")
            print("       the rest of '%s'.  Aborting."%(filename))
            sys.exit(1)

        # apply functions to columns of a
        for i in range(len(self.all_params)):
            if (self.all_params[i] in config.apply_function):
                function = config.apply_function[self.all_params[i]]
                if (function != "del"): 
                    a[:,i] = function_map[function](a[:,i])
                    self.all_params_latex[i] = insert_function_latex(function, \
                        self.all_params_latex[i])
                    if (self.all_params[i] in self.grid_params):
                        j = self.grid_params.index(self.all_params[i])
                        self.grid_params[j] = function+"_"+self.grid_params[j]
                    self.all_params[i] = function+"_"+self.all_params[i]
                else:
                    # this situation should never occur
                    sys.exit("ERROR: trying to remove non-deleted parameter")

        # find indices of grid parameters
        ndx = utilities.my_map(self.all_params.index,self.grid_params)

        # find ranges of grid parameters and carry out sanity check:
        params_span = np.max(a[:,ndx],axis=0)-np.min(a[:,ndx],axis=0)
        for i in range(len(self.grid_params)):
            span = params_span[i]
            if (np.isnan(span)):
                sys.exit("ERROR: the values of some models parameters are NaN.")
            if (np.isinf(span)):
                sys.exit("ERROR: the values of some models parameters are infinite.")
            if (span == 0.0):
                print("ERROR: parameter %s is constant in your grid. Therefore,"%(self.grid_params[i]))
                print("       it cannot be used as a grid parameter.  Please edit the")
                print("       value of grid_params in the 3rd line of your list file.")
                sys.exit(1)

        # find indices of age parameters
        self.set_age_indices()

        # sort according to ndx
        # NOTE: list conversion is needed for python3
        ind = np.lexsort(utilities.my_map(lambda i: a[:,i], ndx))
        a = a[ind,:]

        # split into different evolutionary tracks
        i0 = 0
        j = 0
        for i in range(1,a.shape[0]):
            val = np.max(np.abs(a[i-1,ndx]-a[i,ndx]))
            if (val == 0.0): continue
            aTrack = Track(a[i0:i,:],a[i0,ndx])
            self.tracks.append(aTrack)
            if (not config.batch):
                print("%d %d"%(j,i))
                print('\033[2A') # backup two line - might not work in all terminals
            j += 1
            i0 = i
        aTrack = Track(a[i0:,:],a[i0,ndx])
        self.tracks.append(aTrack)
        print("%d %d"%(j,a.shape[0]))

        # merge tracks which are too close:
        imerge = 0
        i = 1
        params_span *= eps
        while (i < len(self.tracks)):
            for j in range(i):
                if (np.all(np.abs(self.tracks[i].params-self.tracks[j].params) < params_span)):
                    self.tracks[j].append_track(self.tracks[i])
                    del self.tracks[i]
                    imerge += 1
                    break
            else:
                i+=1
        print("I merged %d track(s)"%(imerge))

        # replace dimensionless age parameter depending on config.replace_age_adim
        self.replace_age_adim()

        # sort tracks and remove duplicate models
        for aTrack in self.tracks:
            aTrack.sort()
            if (not aTrack.is_sorted()):  # sanity check on age parameters
               sys.exit("ERROR: The age parameter does not increase with age.")
            aTrack.remove_duplicates()
 
        # update list of indices:
        self.ndx = range(len(self.tracks))

        # need to create grid from scratch since tracks have been merged.
        self.grid = np.asarray([track.params for track in self.tracks])

    def set_age_indices(self):
        """ Set indices corresponding to physical age and age parameter """

        global iage, iage_adim
        iage = self.all_params.index(age_str)
        iage_adim = self.all_params.index(age_adim_str)

    def replace_age_adim(self):
        """
        This replaces the dimensionless ages in the tracks according to the
        :py:data:`replace_age_adim` option chosen in ``AIMS_configure.py``.
        """

        if (config.replace_age_adim is None):
            return
        elif (config.replace_age_adim == "scale_age"):
            for track in self.tracks:
                track.age_adim_to_scaled_param(iage,forward=True)
        elif (config.replace_age_adim == "scale_Xc"):
            if ("Xc" in self.all_params):
                ixc = self.all_params.index("Xc")
            else:
                print('ERROR: "Xc" is not a grid parameter.  It cannot be')
                print('       used to calculate the dimensionless age.')
                sys.exit(1)
            for track in self.tracks:
                track.age_adim_to_scaled_param(ixc,forward=False)
        else:
            sys.exit("ERROR: unknown option for replace_scale_age in AIMS_configure.py")

    def prepare_interpolation(self):
        """
        Initialise the variables nmodelsmax, age_array, and age_adim_array
        which are needed for interpolation.
        """
        
        global nmodelsmax, age_array, age_adim_array
        global indices_along_track, weights_along_track

        nmodelsmax = max([track.models.shape[0] for track in self.tracks])
        age_array = np.zeros((nmodelsmax,self.ndim+1),dtype=float)
        age_adim_array = np.zeros((nmodelsmax,self.ndim+1),dtype=float)
        indices_along_track = np.zeros((self.ndim+1,3),dtype=int)
        weights_along_track = np.zeros((self.ndim+1),dtype=float) 

    def write_list_file(self,filename):
        """
        Write list file from which to generate binary grid.  Various filters
        can be included to reduce the number of models.

        :param filename: the name of the output list file
        :type filename: str

        .. note::
          This code is intended for developpers not first time users.
        """

        # write results to file
        output = open(filename,"ab")
        output.write("# "+str(self.all_params)+" # all_params\n")
        output.write("# "+str(self.all_params_latex)+" # all_params_latex\n")
        output.write("# "+str(self.grid_params)+" # grid_params\n")
        for track in grid.tracks:
            #if (int(round(track.params[0]*100.0))%4 != 0): continue # impose step of 0.04 Msun
            np.savetxt(output,track.models)
        output.close()

    def print_header(self):
        """
        Print the header for the grid, namely the variables all_params, all_params_latex,
        and grid_params.
        """

        print("# "+str(self.all_params)+" # all_params\n")
        print("# "+str(self.all_params_latex)+" # all_params_latex\n")
        print("# "+str(self.grid_params)+" # grid_params\n")

    def range(self,aParam):
        """
        Find range of values for the input parameter.

        :param aParam: name of the parameter for which to find the range
        :type aParam: str
        """

        if (aParam == age_adim_str):
            param_min = self.tracks[0].models[0,iage_adim]
            param_max = self.tracks[0].models[-1,iage_adim]

            for track in self.tracks:
                if (param_min > track.models[0,iage_adim]):  param_min = track.models[0,iage_adim]
                if (param_max < track.models[-1,iage_adim]): param_max = track.models[-1,iage_adim]

        elif (aParam == age_str):
            param_min = self.tracks[0].models[0,iage]
            param_max = self.tracks[0].models[-1,iage]

            for track in self.tracks:
                if (param_min > track.models[0,iage]):  param_min = track.models[0,iage]
                if (param_max < track.models[-1,iage]): param_max = track.models[-1,iage]

        else:
            if (aParam not in self.grid_params):
                sys.exit("Cannot find range on unknown quantity %s."%(aParam))

            i = self.grid_params.index(aParam)
            param_min = self.tracks[0].params[i]
            param_max = self.tracks[0].params[i]

            for track in self.tracks:
                if (param_min > track.params[i]): param_min = track.params[i] 
                if (param_max < track.params[i]): param_max = track.params[i] 

        return [param_min, param_max]

    def age_range(self,pt):
        """
        Find age range for a given set of structural parameters in the grid.

        :param pt: input set of parameters (age is included for convenience but not used)
        :type pt: 1D numpy float array

        :return: the age range (min and max value)
        :rtype: 1D numpy float array
        """

        coefs, tracks = find_interpolation_coefficients(self,pt,self.tessellation,self.ndx)
        arr1 = [track.models[0,iage_adim] for track in tracks]
        arr2 = [track.models[-1,iage_adim] for track in tracks]
        lower_age_adim = max([track.models[0,iage_adim] for track in tracks])
        upper_age_adim = min([track.models[-1,iage_adim] for track in tracks])
        result = np.zeros((2,),dtype=gtype)
        for i in range(len(coefs)):
            result[0] += coefs[i]*tracks[i].interpolate_model(lower_age_adim)[iage]
            result[1] += coefs[i]*tracks[i].interpolate_model(upper_age_adim)[iage]
        return result

    def string_to_latex(self,string):
        """
        Find latex string representation of input string by looking
        in the self.all_params_latex list.

        :param string: the string for which a latex representation is needed
        :type string: str

        :return: the latex representation of the input string
        :rtype: str
        """

        fundamental = extract_fundamental_parameter(string)
        if (fundamental not in self.all_params_fundamental):
            print("ERROR: %s not found in grid.all_params_fundamental"%(fundamental))
            return None

        ndx = self.all_params_fundamental.index(fundamental)
        lst = extract_functions(string)
        lst2 = extract_functions(self.all_params[ndx])
        lst3, reduced_name = extract_functions_latex(self.all_params_latex[ndx])
        lst = simplify_functions(lst+invert_functions(lst2)+lst3)
        return insert_functions_latex(lst,reduced_name)

    def string_to_index(self,string):
        """
        Find array/list index which corresponds to the parameter represented
        by the string, by looking in the self.all_params_latex list.

        :param string: the string representing the parameter
        :type string: str

        :return: the index corresponding to the parameter
        :rtype: int
        """

        if (string not in self.all_params):
            print("ERROR: %s not found in grid.all_params"%(string))
            return None
        return self.all_params.index(string)

    def string_to_param(self,string):
        """
        Find array/list index which corresponds to the parameter represented
        by the string, by looking in the self.all_params_latex list, as
        well as the list of functions needed to convert a model quantity
        to the quantity represented by the string.

        :param string: the string representing the parameter
        :type string: str

        :return: the index corresponding to the parameter, and a list of functions
        :rtype: int, list of functions
        """

        fundamental = extract_fundamental_parameter(string)
        if (fundamental not in self.all_params_fundamental):
            print("ERROR: %s not found in grid.all_params_fundamental"%(fundamental))
            return None

        ndx = self.all_params_fundamental.index(fundamental)
        lst = extract_functions(string)
        lst2 = extract_functions(self.all_params[ndx])
        lst = simplify_functions(lst + invert_functions(lst2))
        return ndx, [function_map[x] for x in lst]

    def string_to_param_extended(self,string):
        """
        Find array/list index which corresponds to the parameter represented
        by the string, by looking in the self.all_params_latex list, as
        well as the list of functions needed to convert a model quantity
        to the quantity represented by the string and the inverted list
        of functions.  Also provides the set of derivative functions
        (used for a change of variables in probability theory),
        and the inverted set of functions.

        :param string: the string representing the parameter
        :type string: str

        :return: the index corresponding to the parameter, list of
                 functions to produce string, list of derivative functions,
                 and inverted list of functions
        :rtype: int, list of functions, list of functions, list of functions
        """

        fundamental = extract_fundamental_parameter(string)
        if (fundamental not in self.all_params_fundamental):
            print("ERROR: %s not found in grid.all_params_fundamental"%(fundamental))
            return None

        ndx = self.all_params_fundamental.index(fundamental)
        lst = extract_functions(string)
        lst2 = extract_functions(self.all_params[ndx])
        lst = simplify_functions(lst + invert_functions(lst2))
        return ndx, [function_map[x] for x in lst], \
               [function_map_der[x] for x in lst],  \
               [function_map[x] for x in invert_functions(lst)]

    def tessellate(self):
        """Apply Delauny triangulation to obtain the grid tessellation."""

        if (self.distort_mat is None):
            self.tessellation = Delaunay(self.grid)
        else:
            self.tessellation = Delaunay(np.dot(self.grid,self.distort_mat))

    def plot_tessellation(self):
        """
        Plot the grid tessellation.

        .. warning::
          This only works for two-dimensional tessellations.
        """

        if (self.ndim != 2):
            print("Only able to plot the tessellation in two dimensions.")
            return

        # find bounds:
        xmin = np.nanmin(self.grid[:,0])
        xmax = np.nanmax(self.grid[:,0])
        ymin = np.nanmin(self.grid[:,1])
        ymax = np.nanmax(self.grid[:,1])
        dx = xmax-xmin
        dy = ymax-ymin
        xmin -= dx*0.03
        xmax += dx*0.03
        ymin -= dy*0.05
        ymax += dy*0.05

        plt.plot(self.grid[:,0],self.grid[:,1],'o')
        plt.triplot(self.grid[:,0],self.grid[:,1],self.tessellation.simplices.copy())
        plt.xlim((xmin,xmax))
        plt.ylim((ymin,ymax))
        plt.xlabel(self.string_to_latex(self.grid_params[0]))
        plt.ylabel(self.string_to_latex(self.grid_params[1]))
        plt.savefig("tessellation.eps")

    def distort_grid(self):
        """
        Define distortion matrix with which to distort grid to break its Cartesian 
        character prior to tessellation.  This can cause find_simplex to run much
        faster.
        """

        self.distort_mat = np.dot(make_scale_matrix(self.grid),make_distort_matrix(self.ndim))
        print("Distortion matrix condition number: %e"%(np.linalg.cond(self.distort_mat)))

    def test_interpolation(self):
        """
        Test interpolation between different evolutionary tracks in a given grid.

        :return: The following four items are returned:

          - the interpolation errors
          - the first half of the partition (where the interpolation is tested)
          - the second half of the partition (used to carry out the interpolation)
          - the tessellation associated with the second half of the partition

        :rtype: np.array, list, list, tessellation object
        """

        ndx1, ndx2 = self.find_partition()
        tessellation = Delaunay(self.grid[ndx2,:])

        # initialisation
        results = []
        ndim = self.ndim+1

        for j in ndx1:
            nmodels = len(self.tracks[j].models)
            aResult = np.empty((nmodels,ndim+nglb),dtype=gtype)
            pt = np.concatenate((self.tracks[j].params,np.asarray([0.0,])))

            for i in range(nmodels):
                aModel1 = self.tracks[j].models[i,:]
                pt[-1] = aModel1[iage]
                aModel2 = interpolate_model(self,pt,tessellation,ndx2)
                aResult[i,0:ndim-1] = pt[:-1]
                aResult[i,ndim-1] = aModel1[iage_adim]
                if (aModel2 is None):
                    aResult[i,ndim:ndim+nglb] = np.nan
                else:
                    aResult[i,ndim:ndim+nglb] = compare_models(aModel1,aModel2)

            results.append(aResult)

        return results, ndx1, ndx2, tessellation

    def find_partition(self):
        """
        Find a partition of the grid for use with :py:meth:`Model_grid.test_interpolation`

        :return: a random partition of [0 ... n-1] into two equal halves, where n is
                 the number of tracks in the grid
        :rtype: two lists of int

        """

        ndx = list(range(len(self.tracks)))
        random.shuffle(ndx)
        nn = len(self.tracks)//2
        return ndx[:nn],ndx[nn:]

    def find_isochrone(self, parameters, npts):
        """
        This calculates an isochrone and writes it to a file.

        :param parameters: the parameters used in determining the isochrone
                           (typically all of grid parameters except for mass)
        :type parameters: dict

        :param npts: maximum number of points in isochrone.  The actual number of
                     points may be less as a result of the target age being outside
                     the evolutionary tracks for some of the masses.
        :type npts: int

        :return: table of interpolated models along isochrone
        :rtype: 2D numpy float array
        """

        # preliminary settings
        grid_params = self.grid_params + [age_str,]
        grid_params_fundamental = utilities.my_map(extract_fundamental_parameter, \
                                  grid_params)
        nparams = len(grid_params)
        ndim = nparams-1 # number of dimensions in grid (excluding age dimension)

        # extract mass index
        params_temp = utilities.my_map(str.lower,grid_params_fundamental)
        if ("mass0" in params_temp):
            imass = params_temp.index("mass0")
        elif ("mass" in params_temp):
            imass = params_temp.index("mass")
        else:
            print("ERROR: Your grid does not seem to include stellar mass.")
            print("       Unable to calculate an isochrone.")
            return None

        # make sure all relevant parameters are supplied once and once only in
        # the parameters dictionary, and load relevant parameters in params array
        params = np.empty((nparams,),dtype=float)
        params[:] = np.nan
        for key in parameters:
            key_fundamental = extract_fundamental_parameter(key)
            if (key_fundamental not in grid_params_fundamental):
                print("WARNING: parameter \"%s\" unused in finding isochrone."%(key))
                continue
            i = grid_params_fundamental.index(key_fundamental)
            if (i == imass):
                print("ERROR: do not specify the mass for an isochrone.")
                return None
            if (not np.isnan(params[i])):
                print("ERROR: do not specify the same parameter twice for an isochrone.")
                return None
            key_extended = self.string_to_param_extended(key)
            params[i] = apply_functions(key_extended[3],parameters[key])

        # find mass range for the isochrone:
        print("Finding isochrone mass range")
        mass_range = []
        params_copy = params[:-1].copy() # must remove age
        params_copy[imass] = 1.0
        facet = np.empty((ndim,ndim),dtype=gtype)
        nfacets = self.tessellation.convex_hull.shape[0] # number of facets in the convex hull
        for i in range(nfacets):
            for j in range(ndim):
                facet[j,:] = self.tessellation.points[self.tessellation.convex_hull[i,j]]
            mat = np.transpose(facet.copy())
            mat[imass,:] = 1.0
            try:
                # the matrix may be singular, especially in Cartesian grids
                coefs = np.linalg.solve(mat,params_copy)
            except np.linalg.linalg.LinAlgError:
                continue
            if (np.any(coefs < -tol)): continue
            mass_range.append(np.dot(coefs,facet[:,imass]))
        min_mass = min(mass_range)
        max_mass = max(mass_range)
        dmass = max_mass-min_mass
        # choose points slightly within convex hull to avoid accientally falling outside
        min_mass += tol*dmass
        max_mass -= tol*dmass

        # find isochrone
        print("Finding isochrone")
        nmissing = 0
        result = []
        for i in tqdm(range(npts),total=npts):
            params[imass] = ((npts-i-1)*min_mass+i*max_mass)/(npts-1)
            aModel = interpolate_model(self,params,self.tessellation,self.ndx)
            # the target age may lie outside the interpolated track, hence the
            # need to test the model
            if (aModel is None):
                nmissing += 1
            else:
                result.append(aModel)
        print("WARNING: %d models are missing or beyond the age range in the isochrone."%(nmissing))

        return np.array(result)

def make_distort_matrix(d,theta=0.157):
    """
    Create a distortion matrix which can be used to make the grid more
    "tessellation-friendly", i.e. which leads to much shorter computation
    times for finding simplices.

    :param d: number of dimensions
    :type d: int

    :param theta: a small angle
    :type theta: float

    :return: a distortion matrix
    :rtype: 2D float array
    """
    cost = math.cos(theta)
    sint = math.sin(theta)
    mat = np.eye(d)
    aux = np.empty((d,d),dtype=float)
    for i in range(d):
        for j in range(i+1,d):
            aux[:,:] = 0.0
            for k in range(d):
                if ((k == i) or (k == j)): continue
                aux[k,k] = 1.0
            aux[i,i] = 1.0
            aux[j,j] = cost
            aux[j,i] = 0.0
            aux[i,j] = sint
            mat = np.dot(mat,aux)

    return mat

def make_scale_matrix(grid):
    """
    Create a distortion matrix which can be used to make the grid more
    "tessellation-friendly", i.e. which leads to much shorter computation
    times for finding simplices.

    :param grid: set of points used in the construction of the tessellation
    :type d: 2D float array

    :return: a distortion matrix
    :rtype: 2D float array
    """

    eps = 1e-3
    d = grid.shape[1]
    mat = np.eye(d)
    for i in range(d):
        x = np.asarray(list(set(grid[:,i])))
        x = np.sort(x)
        x = x[1:]-x[:-1]
        x = np.sort(x)
        ind = bisect_right(x,eps)
        mat[i,i] = 1.0/x[ind]
        print ("Scale factor %d: %e"%(i,mat[i,i]))
    return mat 

def simplify_name(string):
    """
    Simplify a string representation of a variable including functions,
    by cancelling out inverse functions.

    :param string: the string to be simplified
    :type string: str

    :return: the simplified string
    :rtype: str
    """

    lst = simplify_functions(extract_functions(string))
    if (len(lst) > 0):
        return "_".join(lst)+"_"+extract_fundamental_parameter(string)
    else:
        return extract_fundamental_parameter(string)

def extract_functions(name):
    """
    Extract the functions applied within the "name" variable.

    :param name: the name of the variable to be split
    :type name: str

    :return: a list with the string representation of functions to be applied and
             the name of the fundamental quantity
    :rtype:  list of str
    """
    i = name.find("_")
    if (i == -1): return []
    if (name[:i] in function_map):
        return [name[:i],]+extract_functions(name[i+1:])
    else:
        return []

def extract_fundamental_parameter(name):
    """
    Extracts the fundamental parameter from the "name" variable,
    i.e. the quantity obtained after removal of different functions
    applied to it.

    :param name: the name of the variable from which to extract the fundamental
                 quantity
    :type name: str

    :return: the fundamental quantity
    :rtype:  list of str
    """

    i=0
    while(True):
        j = name[i:].find("_")
        if (j == -1): return name[i:]
        j += i
        if (name[i:j] in function_map):
            i = j + 1
        else:
            return name[i:]

def simplify_functions(lst):
    """
    Go through a list of string representations of functions and simplify
    consecutive functions which are the inverse of each other.

    :param lst: a list of functions and the fundamental quantity
    :type lst:  list of str

    :return: a simplified list of functions with the fundamental quantity
    :rtype:  list of str
    """
    i = 0
    while (i < len(lst)-1):
        if (lst[i+1] == function_inv[lst[i]]):
            del lst[i+1]
            del lst[i]
            if (i > 0): i -= 1
        else:
            i+=1

    return lst 

def apply_functions(funcs, x):
    """
    Apply a set of functions to array x.

    :param funcs: list of functions to apply to x
    :type funcs: list of functions

    :param x: value to which to apply the function
    :type x: float

    :return: the functions applied to x
    :rtype: float 
    """ 

    result = x
    for i in range(len(funcs)-1,-1,-1):
        result = funcs[i](result)
    return result

def apply_functions_der(funcs, funcs_der, x):
    """
    Apply a set of functions to array x.  Find corresponding
    derivative (as a function of x).

    :param funcs: list of functions to apply to x
    :type funcs: list of functions

    :param funcs_der: list of derivatives to apply to x
    :type funcs_der: list of functions

    :param x: value to which to apply the function
    :type x: float

    :return: the functions applied to x and the derivative
    :rtype: (float, float)
    """ 

    fvalue = x
    fder   = 1.0
    for i in range(len(funcs)-1,-1,-1):
        fder  *= funcs_der[i](fvalue)
        fvalue = funcs[i](fvalue)
    return fvalue, fder

def insert_function_latex(func,name_latex):
    """
    Insert a fancy latex function name into a latex representation
    of a variable in a string.

    :param func: the name of the function
    :type func: str

    :param name_latex: the string variable with the latex part
    :type func: str

    :return: a modified string variable 
    :rtype: str

    .. note:
      This method assumes the latex part is enclosed in dollars ($).
      If there are no dollar signs in the string, then the resultant
      string will be the same as the input string.
    """

    if (func not in function_map): return name_latex
    i = name_latex.find("$")
    if (i == -1): return name_latex
    j = name_latex[i+1:].find("$")
    if (j == -1): return name_latex
    j += i+1
    return name_latex[:i+1]+function_left[func] \
          +name_latex[i+1:j]+function_right[func] \
          +name_latex[j:]

def insert_functions_latex(funcs,name_latex):
    """
    Insert fancy latex function names into a latex representation
    of a variable in a string.

    :param funcs: the names of the functions
    :type funcs: list of str

    :param name_latex: the string variable with the latex part
    :type func: str

    :return: a modified string variable 
    :rtype: str

    .. note:
      This method assumes the latex part is enclosed in dollars ($).
      If there are no dollar signs in the string, then the resultant
      string will be the same as the input string.
    """

    simplify_functions(funcs)
    result = name_latex
    for i in range(len(funcs)-1,-1,-1):
        result = insert_function_latex(funcs[i],result)
    return result

def extract_function_latex(name_latex):
    """
    Extract a fancy latex function name (if any) from a latex
    representation of a variable in a string.

    :param name_latex: the string variable with the latex part
    :type func: str

    :return: a string with the extracted function (or None), and
             and a string representation of the variable without
             the function
    :rtype: str,str

    .. note:
      This method assumes the latex part is enclosed in dollars ($).
      If there are no dollar signs in the string, then no functions
      can be extracted.
    """

    i = name_latex.find("$")
    if (i == -1): return None, name_latex
    j = name_latex[i+1:].find("$")
    if (j == -1): return None, name_latex
    j += i+1
    for func in function_map:
        if (name_latex[i+1:j].startswith(function_left[func]) and
            name_latex[i+1:j].endswith(function_right[func])):
            reduced_name = name_latex[:i+1]                         \
                         + name_latex[i+1+len(function_left[func]): \
                                      j-len(function_right[func])]  \
                         + name_latex[j:]
            return func, reduced_name
    return None, name_latex

def extract_functions_latex(name_latex):
    """
    Extract fancy latex function names (if any) from a latex
    representation of a variable in a string.

    :param name_latex: the string variable with the latex part
    :type func: str

    :return: a list of extracted functions, and and a string
             representation of the variable without the functions
    :rtype: list of str,str

    .. note:
      This method assumes the latex part is enclosed in dollars ($).
      If there are no dollar signs in the string, then no functions
      can be extracted.
    """

    funcs = []
    reduced_name = name_latex
    while(True):
        func, reduced_name = extract_function_latex(reduced_name)
        if (func is None):
            return simplify_functions(funcs), reduced_name
        funcs.append(func)

def invert_functions(funcs):
    """
    Produces a list of inversed functions in reverse order.  Hence,
    the composition of the input argument with the result leads to
    the identity function.

    :param funcs: list of functions to be inverted and reversed
    :type funcs: list of str

    :return: reversed list of inverted functions
    :rtype: list of str
    """

    return [function_inv[func] for func in reversed(funcs)]

def compare_models(model1,model2):
    """
    Compare two models and find the largest frequency different for
    radial and non-radial modes.

    :param model1: first model
    :param model2: second model

    :type model1: :py:class:`Model`
    :type model2: :py:class:`Model`

    :return: a 1D array with the differences on the global parameters
    :rtype: np.array
    """

    return np.fabs(model1 - model2)

def find_point_in_convex_hull(pt, ref_pt, tessellation, distort_mat=None):
    """
    For a given point, find point within the convex hull of a tessellation,
    which lies on the segment connecting this point to another reference
    point, and is closest to the original point.

    This uses dichotomy to find the boundary between the inside and outside
    of the convex hull, if needed.

    :param pt: input point for which we are searching for a corresponding point
               in the convex hull
    :type pt: 1D numpy float array

    :param ref_pt: reference point used to define relevant segment
    :type ref_pt: 1D numpy float array

    :param tessellation: tessellation in which we are searching
      for point in convex hull
    :type tessellation: tessellation object

    :param distort_mat: distortion matrix that was used prior to the tessellation
                        (or None if there is no distortion)
    :type distort_mat: 2D numpy float array
 
    :return: the point within the convex hull nearest to the input point
    :rtype: 1D numpy float array

    .. note::
        If the original point is already in the convex hull, this
        point is simply returned.
    """

    # take grid distortion into account if need be:
    if (distort_mat is None):
        pt0 = pt
        ref_pt0 = ref_pt
    else:
        pt0 = np.dot(pt,distort_mat)
        ref_pt0 = np.dot(ref_pt,distort_mat)

    # sanity check (this should not happen):
    if (tessellation.find_simplex(ref_pt0) == -1):
        print("ERROR: an unexpected error occurred in find_point_in_convex_hull.")
        print("       Please contact the authors of SPInS.")
        sys.exit(1)

    # easy exit:
    if (tessellation.find_simplex(pt0) != -1): return pt

    # the general case:
    pt1 = pt0.copy()
    pt2 = ref_pt0.copy()
    threshold = eps*np.linalg.norm(pt2-pt1)

    # dichotomic search for relevant bound
    while (np.linalg.norm(pt2-pt1)>threshold):
        pt3 = (pt1+pt2)/2.0
        if (tessellation.find_simplex(pt3) == -1):
            pt1 = pt3
        else:
            pt2 = pt3

    # to be on the safe side, return the point that is marginally in
    # the convex hull:
    if (distort_mat is None):
        return pt2
    else:
        return np.linalg.solve(distort_mat.T,pt2.T).T

def find_interpolation_coefficients(grid,pt,tessellation,ndx):
    """
    Find interpolation weights from the corresponding simplex.

    Linear interpolation weights are obtained with the simplex
    by finding the barycentric coordinates of the point given
    by ``pt``.

    :param grid: grid of models in which we're carrying out the
      interpolation
    :param pt: set of parameters used for finding the
      interpolation weights.  The first part contains the grid
      parameters (relevant to this interpolation), whereas
      the last element is the age (not used here).  If the
      provided set of parameters lies outside the grid, then
      ``None`` is returned instead of an interpolated model.
    :param tessellation: tessellation with which to carry out the
      interpolation.
    :param ndx: indices of the grid points associated with the
      tessellation

    :type grid: :py:class:`Model_grid`
    :type pt: array-like
    :type tessellation: tessellation object
    :type ndx: list of int

    :return: lists of interpolation coefficients and tracks
    :rtype: list of floats, list of :py:class:`Track`
    """

    if (pt is None): return None, None
    if (tessellation is None): return None, None
    if (grid.distort_mat is None):
        pt1 = np.asarray(pt[0:-1],dtype=gtype)
    else:
        pt1 = np.dot(np.asarray(pt[0:-1],dtype=gtype),grid.distort_mat)
    val = tessellation.find_simplex(pt1.reshape((1,grid.ndim)))[0]

    # see if point is outside tessellation
    if (val == -1): return None, None
    mat = tessellation.transform[val]

    # make sure the transformation matrix is defined:
    if (math.isnan(np.sum(mat))): return None, None

    b = mat[:grid.ndim].dot(pt1-mat[grid.ndim])
    coefs = np.r_[b, 1.0-b.sum()]
    ind = tessellation.simplices[val]

    # check to make sure you're not outside the grid:
    for coef in coefs:
        if (coef < -tol): return None, None

    # produce results, filtering out zero elements:
    coefs_out = []
    tracks = []
    for coef,i in zip(coefs,ind):
        # remove negative coefficients to avoid problems.
        if (coef > 0.0):
            coefs_out.append(coef)
            tracks.append(grid.tracks[ndx[i]])

    del pt1, mat, b, coefs, ind
    return coefs_out, tracks

def interpolate_model(grid,pt,tessellation,ndx):
    """
    Interpolate model in grid using provided parameters.

    The interpolation is carried out in two steps.  First, linear
    interpolation according to age is carried out on each node of
    the simplex containing the set of parameters.  This interpolation
    is done using the :py:class:`Track.interpolate_model` method.
    Then, linear interpolation is carried out within the simplex.
    This achieved by finding the barycentric coordinates of the
    model (i.e. the weights), before combining the age-interpolated
    models form the nodes using the :py:class:`combine_models` method.
    In this manner, the weights are only calculated once, thereby
    increasing computational efficiency.

    :param grid: grid of models in which we're carrying out the
      interpolation
    :param pt: set of parameters used for the interpolation.
      The first part contains the grid parameters, whereas
      the last element is the age.  If the provided set
      of parameters lies outside the grid, then ``None``
      is returned instead of an interpolated model.
    :param tessellation: tessellation with which to carry out the
      interpolation.
    :param ndx: indices of the grid points associated with the
      tessellation

    :type grid: :py:class:`Model_grid`
    :type pt: array-like
    :type tessellation: tessellation object
    :type ndx: list of int

    :return: the interpolated model
    :rtype: :py:class:`Model`
    """

    # find simplex interpolation coefficients
    coefs,tracks = find_interpolation_coefficients(grid,pt,tessellation,ndx)
    if (coefs is None): return None

    # sanity check:
    coefs = np.array(coefs)
    scoefs = np.sum(coefs)
    if (abs(scoefs-1.0) > eps):
        print("WARNING: interpolation coefficients not yet normalised. Fixing ...")
        coefs /= scoefs

    global nmodelsmax, age_array, age_adim_array
    global indices_along_track, weights_along_track

    ntracks = len(tracks)
    sze = np.array([tracks[j].models.shape[0] for j in range(ntracks)],dtype=int)
    for i in range(ntracks):
        nmodels = sze[i]
        age_array[0:nmodels,i] = tracks[i].models[:,iage]
        age_adim_array[0:nmodels,i] = tracks[i].models[:,iage_adim]

    age_adim = np.nan
    if (config.synchronised):
        age_adim, ndx, weight = spins_fortran.find_tau_synchronised(age_adim_array[:,0], \
            age_array[:,0:ntracks], coefs, sze, pt[-1], age_adim)
        if (math.isnan(age_adim)): return None
        indices_along_track[:,0] = ndx
        weights_along_track[:] = weight
    else:
        age_adim, indices_along_track[0:ntracks,:], weights_along_track[0:ntracks] =   \
            spins_fortran.find_tau(age_adim_array[:,0:ntracks], age_array[:,0:ntracks], coefs, \
            sze, pt[-1], age_adim, indices_along_track[0:ntracks,:], weights_along_track[0:ntracks])
        if (math.isnan(age_adim)): return None

    # sanity check:
    if (indices_along_track[0,0] == -1): return None

    # treat the case where there are at least 2 models:
    aModel1 = combine_models(tracks[0].models[indices_along_track[0,0]-1,:],weights_along_track[0], \
        tracks[0].models[indices_along_track[0,0],:],1.0-weights_along_track[0])
    if (ntracks == 1): return aModel1
    aModel2 = combine_models(tracks[1].models[indices_along_track[1,0]-1,:],weights_along_track[1], \
        tracks[1].models[indices_along_track[1,0],:],1.0-weights_along_track[1])
    aModel1 = combine_models(aModel1,coefs[0],aModel2,coefs[1])
    del aModel2
    for i in range(2,ntracks):
        aModel2=combine_models(tracks[i].models[indices_along_track[i,0]-1,:],weights_along_track[i], \
            tracks[i].models[indices_along_track[i,0],:],1.0-weights_along_track[i])
        aModel1 = combine_models(aModel1,1.0,aModel2,coefs[i])
        del aModel2

    return aModel1

def combine_models(model1,coef1,model2,coef2):
    """
    Do linear combination of two models.

    This method returns a new model which is the weighted sum or
    average of two models for the purposes of model interpolation.

    :param model1: first model
    :param coef1:  weighting coefficient applied to first model
    :param model2: second model
    :param coef2:  weighting coefficient applied to second model

    :type model1: np.array
    :type coef1:  float
    :type model2: np.array
    :type coef2:  float

    :return: the combined model
    :rtype: np.array
    """

    return coef1*model1 + coef2*model2
