#!/usr/bin/env python
# coding: utf-8
# $Id: analyse_grid.py
# Author: Daniel R. Reese <daniel.reese@obspm.fr>
# Copyright (C) Daniel R. Reese and Yveline Lebreton
# Copyright license: GNU GPL v3.0
#
#   SPInS is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with SPInS.  If not, see <http://www.gnu.org/licenses/>.
#

"""
A simple utility for analysing binary SPInS grids

To analyse a binary SPInS grid, simply type the following command in
the same folder as the SPInS installation::

  ./analyse_grid.py your_binary_grid

"""

import sys
import dill
import numpy as np

def print_number_of_models_in_tracks(grid):
    """
    Prints the number of models in each evolutionary track in a SPInS grid.

    :param grid: the SPInS grid 
    :type grid: :py:class:`Model_grid`
    """
    for i in range(len(grid.tracks)):
        print("Track %d contains %d models"%(i,len(grid.tracks[i].models)))

def find_range(grid,i):
    """
    Finds range of values on parameter i.

    :param grid: the SPInS grid for which we're searching for the parameter range
    :type grid: :py:class:`Model_grid`

    :param i: the index of the parameter
    :type i: int
    """ 
    param_min = grid.tracks[0].params[i]
    param_max = grid.tracks[0].params[i]

    for track in grid.tracks:
        if (param_min > track.params[i]): param_min = track.params[i] 
        if (param_max < track.params[i]): param_max = track.params[i] 

    print("Range on parameter %s: %8.5e to %8.5e"%(grid.grid_params[i],param_min,param_max))

if __name__ == "__main__":
    """
    The main program.
    """

    # check number of arguments
    assert (len(sys.argv) > 1), "Usage: analyse_grid.py binary_grid_file"

    input_data = open(sys.argv[1],"rb")
    grid = dill.load(input_data)
    input_data.close()

    ntracks = len(grid.tracks)

    print("Grid parameters:  %s"%(str(grid.grid_params)))
    print("All parameters:   %s"%(str(grid.all_params)))
    print("Number of dims.:  %d"%(grid.ndim+1))
    print("Number of tracks: %d"%(ntracks))
    print("Number of models: %d"%(sum([len(track.models) for track in grid.tracks])))

    for i in range(len(grid.grid_params)):
        find_range(grid,i)

    #print_number_of_models_in_tracks(grid)
