#!/usr/bin/env python
# coding: utf-8
# $Id: SPInS_configure.py
# Author: Daniel R. Reese <daniel.reese@obspm.fr>
# Copyright (C) Daniel R. Reese and Yveline Lebreton
# Copyright license: GNU GPL v3.0
#
#   SPInS is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with SPInS.  If not, see <http://www.gnu.org/licenses/>.
#

import math

#########################   Running SPInS   ################################
# OPTIONS for mode:
#    - "write_grid": write binary grid file from list file
#    - "fit_data": fit observed data to characterise star(s)
#    - "test_interpolation": test the accuracy of interpolation within the
#                       grid of models.  The test results are written in
#                       binary format  in the file specified by the
#                       interpolation_file variable and can be subsequently
#                       analysed using plot_test.py.
#    - "write_isochrone": calculate the isochrone specified by the parameters
#                       in isochrone_parameters and save the result in
#                       a text file specified by isochrone_file
mode        = "fit_data"
batch       = False  # running in batch mode? if yes, deactivate tqdm
rm_existing = True   # True: remove extisting results; False: skip stars with existing results

#########################   Parallelisation   ##############################
# NOTE: this is currently implemented with multiprocessing, which duplicates
#       the memory in each process (though in some systems, it is a virtual
#       copy).  To be more memory efficient, turn off parallelisation using
#       the "parallel" parameter.
nprocesses  = 2      # number of processes (if running in parallel)
parallel    = True   # specifies whether to run in parallel

#########################   EMCEE control parameters   #####################
ntemps      = 10     # number of temperatures
nwalkers    = 10     # number of walkers (this number should be even)
nsteps0     = 1000   # number of burn-in steps
nsteps      = 10000  # number of steps
thin        = 10     # thinning parameter (1 out of thin steps will be kept ...)
thin_comb   = 100    # thinning parameter for output linear combinations of models
PT          = True   # use parallel tempering?
PTadapt     = True   # adapt temperatures in parallel tempering?
test_conv   = True   # automatically test convergence through the autocorrelation time evaluation, and stop the MCMC
nstepeval   = 100    # if testing convergence, set the frequency at which the convergence is evaluated
nstepsmin   = 1000   # if testing convergence, one can set a minimum number of steps to run the MCMC, this is to
                     #  ensure having a PDF with enough statistics to derive the results and percentiles

#########################   Initialisation   ###############################
# OPTIONS for samples_file:
#    - None: do not use samples from file
#    - "path/to/file/with/samples": use samples from a file to initialise the
#      walkers.  This overides the settings below for generating the walkers.
# This can be useful for continuing a previous run.
# WARNING: If using a multi-tempered approach, only the lowest
#          temperature is reproduced correctly.
samples_file = None

tight_ball   = True  # initialise with a tight ball around best solution
max_iter     = 1000  # maximum number of iterations to find walker

# Ranges used around tight ball initialisation for walkers.
# NOTES:
#   - these ranges will be re-centred around the parameters of the
#     best model in the grid, depending on the value of tight_ball_behaviour
#     (see below)
#   - each MCMC parameter should appear no more than once
#   - if an MCMC parameter is missing, SPInS will apply a Gaussian 
#     distribution with a width of 1/30th the parameter range
#   - possible distributions include (see Distribution class in
#     SPInS.py)
#     - "Uniform": [lower_bound, upper_bound]
#     - "Gaussian": [centre, one_sigma_value]
#     - "Truncated_gaussian": [centre, one_sigma_value, truncation/sigma]
#   - "IMF1" and "IMF2" distributions may not be used as the re_centre
#     procedure has not been implemented for them
#   - "Uninformative" distributions may not be used as they cannot produce
#     realisations

tight_ball_range = {}             # do not delete this line
#tight_ball_range["log_Mass0"] = ("Gaussian", [0.27, 0.10])
#tight_ball_range["MoH"]       = ("Gaussian", [0.3, 0.2])
#tight_ball_range["Age"]       = ("Gaussian", [700.0, 500.0])

tight_ball_behaviour = "recentre"
# OPTIONS: "recentre": recentre around best-fitting model
#          "keep:"     do not recentre ranges around best-fitting model
#                      but keep user-provided values

#########################   Model grid   ###################################
replace_age_adim = None           # replaces the dimensionless age parameter in
                                  # your grid.  Options include:
                                  #    - None: do not replace age parameter (i.e.
                                  #            keep what is provided in original file)
                                  #    - scale_age: scales the ages so that they
                                  #            range from 0 to 1
                                  #    - scale_Xc: scales the central X values so
                                  #            that they range from 0 to 1 
retessellate  = False             # retessellate grid (this can be useful
                                  # if the binary grid has been produced by
                                  # an outdated version of numpy ...)
distort_grid  = False             # This distorts the grid by multiplying it by
                                  # a distortion matrix in order to break its
                                  # cartesian character and accelerate
                                  # finding simplices.  This will cause the
                                  # grid to be retessellated.
                                  # NOTE: this option is still experimental
                                  #       and may need some further fine-tuning.
list_grid   = "lists/list_basti_scaled_solar_non_canonical"  # file with list of models and characteristics.
                                  # only used when constructing binary file with
                                  # the model grid (i.e. mode == "write_grid")
binary_grid = "data/basti_scaled_solar_non_canonical"    # binary file with model grid
                                  # this file is written to if mode == "write_grid"
                                  # this file is read from if mode != "write_grid"
synchronised = False              # This specifies whether the age parameters between
                                  # different tracks are "synchronised", i.e. whether
                                  # they take on the same values.

apply_function = {}               # do not delete this line
apply_function["Mass0"]  = "log"
apply_function["Mass"]   = "log"
apply_function["Radius"] = "log"
apply_function["Z"]      = "log"
apply_function["Luminosity"] = "log"
apply_function["Teff"]   = "log"
apply_function["DNu"]    = "log"
apply_function["Numax"]  = "log"
apply_function["g"]     = "log"
#apply_function["Mv"]     = "del"  # apply functions to different variables in the grid data
#apply_function["B-V"]    = "del"  # "pow10", "sqrt", "square" -- this will affect interpolation
apply_function["U-B"]    = "del"   # different options are "ln", "log" (=log_10), "exp",
apply_function["H-K"]    = "del"   # "pow10", "sqrt", "square", "inv"
apply_function["V-I"]    = "del"   # This affects:
apply_function["V-R"]    = "del"   #    * interpolation
apply_function["V-J"]    = "del"   #    * tessellation (if relevant)
apply_function["V-K"]    = "del"   #    * the names of variables (ex: "M" -> "log_M")
apply_function["V-L"]    = "del"   # use "del" to remove a column
#apply_function["MoH"]      = "del"

#########################   Priors    ######################################
# The priors are given in a similar format as the tight-ball ranges above.
# An important difference is that the relevant probability distributions
# will not be recentred or renormalised.
#
# NOTES:
#   - each MCMC parameter should appear no more than once
#   - if an MCMC parameter is missing, SPInS will apply 1 of 2 default
#     distributions:
#       1. an uniformative prior if tight_ball == True
#       2. a uniform prior over the range of relevant parameter if
#          tight_ball == False
#   - other parameters may be included - these will be treated like
#     observational contraints
#   - the functions "log_", "ln_", "pow10_", "exp_", "sqrt_", "square_",
#     "inv_" may be applied to the variables
#   - possible distributions include "Uniform", "Gaussian",
#     "Truncate_gaussian" (see description above) as well as:
#     - "IMF1": [mass0, mass1, alpha]
#     - "IMF2": [mass0, mass1, mass2, alpha1, alpha2]
#     - "Casa2001": []  # MDF from Casagrande et al. (2011)
#     - "Above": [threshold,]
#     - "Below": [threshold,]
#     - "Uniformative": []
#   - "Casa2001", "Above", "Below", "Uninformative" distributions cannot
#      lead to realisations.  Hence, only use such distributions when
#      using a tight ball initialisation of the walkers, or initialising
#      from a set of samples.

priors = {}                      # do not delete this line
#priors["Mass0"] = ("IMF2",[0.07,0.5,150.0,1.30,2.30]) # IMF from Kroupa et al. (2013)
#priors["Mass0"] = ("IMF1",[0.07, 150.0, 2.35]) # IMF from Salpeter (1955)
#priors["Age"]   = ("Uniform", [0.0, 1.38e4]) # age in Myrs
#priors["MoH"] = ("Casa2011",[]) # MDF from Casagrande et al. (2011)

# NOTE: adding the postfix "_multi" to the parameter will cause it to
#       be applied nstars time if the parameter is a common parameter
#       in a multiple star system.
#########################   Common parameters    ###########################
# List of common parameters if fitting more than 1 star simultaneously
#
common_params = ["Age", "MoH"]
#########################   Interpolation tests    #########################
interpolation_file = "interpolation_test_basti"  # Name of the file to which to
                                 # write the results from the interpolation
                                 # tests.  This file can be analysed using
                                 # plot_test.py.
#########################   Isochrone    ###################################
# parameters to be used when calculating the isochrone
isochrone_parameters = {}        # do not delete this line
isochrone_parameters["Age"] = 640.0 
isochrone_parameters["MoH"] = 0.094

isochrone_npts = 100000            # maximum number of points in the isochrone
isochrone_file = "my_isochrone"    # file in which to save the isochrone
#########################   Output   #######################################
# choice of parameters: it depends on your grid.  If not known, try
#                       "analyse_grid.py your_grid"
# applicable functions: "log", "ln", "exp", "pow10", "sqrt", "square", "inv"
# example: "log_Mass" corresponds to log_{10}(Mass)
output_params = ["Age_adim", "Mass", "Radius", "Teff", "Luminosity", "MoH", "Numax", "DNu", "log_g"]
output_dir    = "results"      # name of the root folder with the results
with_chain         = False     # decide whether to save the full MCMC chain
with_walkers       = True      # decide whether to plot walkers
with_distrib_iter  = True      # decide whether to plot distrib_iter
with_histograms    = True      # decide whether to plot histograms
with_big           = True      # decide whether to save: big triangle plot, results, percentiles and
                               # samples, with all parameters for all stars
with_triangles     = True      # decide whether to make triangle plots
with_results       = True      # decide whether to save the statistics and percentiles for all parameters
with_samples       = True      # decide whether to save samples
with_rejected      = False     # decide whether to make triangle plots with accepted/rejected models
with_models        = True      # decide whether to save best grid model, best MCMC model, and best statistical model
with_autocorr      = True      # decide whether to save integrated autocorrelation time series
plot_extensions    = ['pdf']   # extensions (and formats) for all simple plots
tri_extensions     = ['png']   # extensions (and formats) for triangle plots
                               # supported formats (may depend on backend): eps, jpeg,
                               #   jpg, pdf, png, ps, raw, rgba, svg, svgz, tif, tiff
backend            = 'agg'     # matplotlib backend with which to produce plots.
                               # Options (may differ according to installation):
                               #   'pdf', 'pgf', 'Qt4Agg', 'GTK', 'GTKAgg',
                               #   'ps', 'agg', 'cairo', 'MacOSX', 'GTKCairo',
                               #   'WXAgg', 'template', 'TkAgg', 'GTK3Cairo',
                               #   'GTK3Agg', 'svg', 'WebAgg', 'CocoaAgg',
                               #   'emf', 'gdk', 'WX'
                               # None = use default backend
                               # NOTE: some backends (such as 'TkAgg') do not
                               #       work in batch mode.
