Project Summary
===============

Description
-----------

**Name**: "Stellar Parameters INferred Systematically" (*SPInS*)

**History**:
  * SPInS is a spin-off of the `AIMS code <https://lesia.obspm.fr/perso/daniel-reese/spaceinn/aims/>`_ 
    `(Rendle et al. 2019) <https://ui.adsabs.harvard.edu/abs/2019MNRAS.484..771R/abstract>`_ 
    in which the seismic part has been removed (thus reducing the computational
    cost)

**Goals**:
  * estimate stellar parameters and credible intervals/error bars
  * chose a representative set or sample of reference models

**Inputs**:
  * classic constraints (Teff, L, ...), averaged seismic constraints
    (:math:`\Delta \nu`, :math:`\delta \nu`, :math:`\nu_{\mathrm{max}}`,
    averaged ratios, period separations ...), and associated probability
    distributions

**Requirements**:
  * a *precalculated* grid of stellar models including:

    - parameters for the model (M, R, Teff, age, averaged seismic parameters, ...)

**Methodology**:
  * applies an MCMC algorithm based on the python package `emcee <https://emcee.readthedocs.io/en/stable/>`_.
    Relevant articles include:

    - `Bazot et al. (2012, MNRAS 427, 1847) <https://ui.adsabs.harvard.edu/abs/2012MNRAS.427.1847B/abstract>`_
    - `Gruberbauer et al. (2012, ApJ 749, 109) <https://ui.adsabs.harvard.edu/abs/2012ApJ...749..109G/abstract>`_

  * interpolates within the grid of models using Delaunay tessellation
    (from the `scipy.spatial <http://docs.scipy.org/doc/scipy/reference/spatial.html>`_
    package which is based on the `Qhull <http://www.qhull.org/>`_ library)
  * modular approach: facilitates including contributions from different
    people

Authors
-------

  * Daniel R. Reese
  * Yveline Lebreton

Supplementary material
----------------------

  * a more technical :download:`overview <./files/Overview.pdf>` of SPInS
  * a PDF version of this documentation may be downloaded
    :download:`here <./files/SPInS.pdf>`

Copyright information
---------------------

  * the SPInS project is distributed under the terms of the
    `GNU General Public License, version 3 <http://www.gnu.org/licenses/gpl-3.0.en.html>`_
  * a copy of of this license may be downloaded :download:`here <../COPYING>`
    and should also be included in ``SPInS.tgz``
