Download ``SPInS``
==================

  * clone SPInS using the following command::

     git clone git@gitlab.obspm.fr:dreese/spins.git

  * alternatively, download it from the git 
    `website <https://gitlab.obspm.fr/dreese/spins.git>`_
  * alternatively, download it with one of the following links and unpack it:
    `zip <https://gitlab.obspm.fr/dreese/spins/-/archive/master/spins-master.zip>`_
    `tar.gz <https://gitlab.obspm.fr/dreese/spins/-/archive/master/spins-master.tar.gz>`_
    `tar.bz2 <https://gitlab.obspm.fr/dreese/spins/-/archive/master/spins-master.tar.bz2>`_
    `tar <https://gitlab.obspm.fr/dreese/spins/-/archive/master/spins-master.tar>`_
  * This will lead to the creation of a folder called ``spins`` and
    a subfolder called ``spins/doc``.

    - the ``spins`` folder contains the SPInS program; it is from this
      folder that SPInS is launched.
    - the ``spins/doc`` folder is where the documentation is generated.
      Typing ``make html`` within this folder will generate this web
      page in ``spins/doc/_build/html/``.  Typing ``make latexpdf``
      will generate a pdf version of this documentation in
      ``spins/doc/_build/latex/SPInS.pdf``.

