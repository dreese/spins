File formats
============

Format of a file with a list of models and properties:
------------------------------------------------------

  Description:
    * The first three lines are a header.   They specify the values of the
      ``all_params``, ``all_params_latex``, and ``grid_params`` variables which
      provide a list of the parameters provided in the file, a latex version of
      the parameter names, and a list of the parameters which characterise the
      evolutionary tracks, respectively. These variables should be specified in
      python format following a hash.  Optional comments may be included at the
      end, following another hash symbol.  The parameters ``Age_adim`` and
      ``Age`` should be included in ``all_params`` and excluded from
      ``grid_params``.

    * The following section is a large table of values.  Each line corresponds
      to one model in the grid and each column to one of the model parameters as
      specified in the first two lines of the header.

    * Except for the first three lines, the order of the lines does not matter. 
      SPInS will construct evolutionary tracks based on the parameters in the
      ``grid_params`` variable, and sort them according to the age parameter
      (``Age_adim``).  Furthermore, the order of the columns does not matter as
      long as they are coordinated with the ``all_params`` and
      ``all_params_latex`` variables.

  Example:
    Here's an example of a file read by SPInS (via the
    :py:meth:`model.Model_grid.read_model_list` method)::

        # ['Age_adim', 'Age', 'Mass0', 'Luminosity', 'Teff', 'Radius', 'MoH'] # all_params
        # [r'Age parameter, $\tau$', r'Age, $t$ (Myrs)', r'Initial mass, $M_0/M_{\odot}$', r'Luminosity, $L/L_{\odot}$', r'Effective temperature, $T_{\mathrm{eff}}$',r'Radius, $R/R_{\odot}$', r'Metallicity, $\mathrm{[M/H]}$'] # all_params_latex
        # ['Mass0','MoH'] # grid_params
        0.00000e+00 1.00000e-15 6.00000e-01 6.66239e-02 4.00654e+03 5.36736e-01 2.54000e-01
        5.00000e-04 1.79472e+02 6.00000e-01 6.66515e-02 4.00654e+03 5.36847e-01 2.54000e-01
        1.00000e-03 3.58944e+02 6.00000e-01 6.66776e-02 4.00654e+03 5.36952e-01 2.54000e-01
        1.50000e-03 5.38417e+02 6.00000e-01 6.67052e-02 4.00654e+03 5.37064e-01 2.54000e-01
        2.00000e-03 7.17889e+02 6.00000e-01 6.67314e-02 4.00654e+03 5.37169e-01 2.54000e-01


    This grid contains 5 models.  The first column contains the dimensionless
    age, the second the physical age (in Myrs), the third the initial mass, and
    so forth.


Format of a file with observational constraints:
------------------------------------------------

  Description:
    * a collection of lines with classic constraints.  These start with the name
      of the relevant variable (the list of available variables is given by the
      ``all_params`` variable of the grid, as specified in the header of the 
      file with the list of model properties).  Various elementary functions can
      be applied to the variables by including the appropriate prefix in the
      name.  This is then followed by a description of its probability
      distribution function.  This probability distribution function is
      specified in two possible ways:

      - it is implicitly assumed to be Gaussian.  In this situation it is only
        necessary to specify the mean value and the one sigma error bar.  For
        example::

          Teff 6100 80

      - it is explicitly specified (different options are given in
        :py:class:`SPInS.Distribution`)::

          Teff Uniform 6000 6200

    * anything following a ``#`` is a comment

    * the order of the lines does not matter

  Example::

        Teff         5283    44      # this is a (useless) comment
        log_Luminosity -0.303 0.011  # a variable with a function applied to it
        MoH Uniform  -0.39  -0.33    # a non-Gaussian distribution
