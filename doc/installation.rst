Installation
============

SPInS is mostly written in python.  However, a few compute intensive parts of
the code have been written in FORTRAN.  These FORTRAN subroutines are then
integrated into the SPInS code thanks to the `f2py
<https://github.com/pearu/f2py/wiki>`_ project.  Accordingly, these FORTRAN
subroutines need to be compiled before running SPInS.  A Makefile has been
provided for convenience.  Hence, one simply needs to type the command::

    make

The user may change the choice of FORTRAN compiler as well as the compilation
options by editing the Makefile.

