Usage
=====

There are four different ways of using SPInS:

  1. generating a binary file with the grid of model parameters

     .. note::
        This step must be carried out before the following three steps
        as these require the above binary file to function correctly.

  2. finding the properties of an observed star thanks to its classic
     and seismic parameters.
  3. carrying out tests to evaluate the accuracy of the interpolation
     for a given grid of models.
  4. generating an isochrone

The way SPInS is used is decided by the values given in the ``SPInS_configure.py``
file, which also contains a number of other control parameters.  Extensive
comments are included in this file to help the user know how to set the various
parameters.

Generating a binary grid
------------------------

  Requirements:

    * an ascii file with two parts:

      - a 3 line header which provides definitions for the variables
        ``all_params``, ``all_params_latex``, ``grid_params``
      - a table containing the parameters of the models in the grid

  Relevant parameters in ``SPInS_configure.py``:

    * ``mode``: set this to ``write_grid`` so that SPInS will write the binary grid.
    * ``replace_age_adim``: allows the user to replace the dimensionless age parameter
      when constructing the binary grid.  This can be useful if the dimensionless
      age parameter is lacking from the grid (in which case SPInS adds zeros for
      this parameter).
    * ``distort_grid``: setting this to ``True`` will distort the grid
      using a linear transformation prior to carrying out the tessellation. In
      most cases, this is not necessary, but in some cases, it may help reduce
      search times, in particular for near-cartesian grids with 4 or more
      dimensions (excluding age).
    * ``list_grid``: set this to the filename of the file with the list of
      paths and global parameters.
    * ``binary_grid``: set this to the filename of the file which will contain
      the binary data.
    * ``synchronised``: set this to ``True`` only if the evolutionary tracks
      in your grid use the same set of values for the age parameter, i.e. the
      grid is "synchronised".  This will cause SPInS to use a slightly simpler
      and faster search algorithm during the age interpolation, but which only
      applies to synchronised grids.
    * ``apply_function``: a python dictionary which allows the user to modify
      different model variables by either applying various elementary functions
      to them or by suppressing them, prior to creating the binary file.
    
  To run SPInS in this configuration, just type the following command in a
  terminal window::

    ./SPInS.py

Characterising an observed star
-------------------------------

  Requirements:
    * a binary grid of models as produced by SPInS
    * one or more file(s) with the observational data; the format for this file is
      is described below.  It will be read by
      :py:meth:`SPInS.Likelihood.read_constraints`

  Relevant parameters in ``SPInS_configure.py``:
    * ``mode``: set this to ``fit_data`` to characterise star(s) using observational
      data 
    * ``distort_grid``: same as above.  If the user changes the value of
      this parameter compared to when the binary grid was constructed, this
      will override the option previously chosen, and will cause the
      grid tessellation to be recalculated if necessary.
    * ``retessellate``: set this to ``True`` if you want to recalculate the
      grid tessellation prior to characterising the observed star.  This can
      be useful for running SPInS with binary grids generated using other
      versions of `scipy <http://www.scipy.org/>`_.
    * ``test_interpolation``: set this to ``False``
    * ``common_params``: list of common parameters when characterising
      multiple systems.  For example, one can impose a common age and
      metallicity in binaries or stellar clusters.
    * most of the parameters in this file - see comments for details

  To run SPInS in this configuration, just type the following command in a
  terminal window::

    ./SPInS.py file_with_constraints

  where ``file_with_constraints`` is the file with the observational
  constraints.  If you are characterising multiple stars with common parameters,
  just type the following command in a terminal window::

    ./SPInS.py file1_with_constraints file2_with_constraints [ ... ]

  where ``file1_with_constraints``, ``file2_with_constraints`` and any additional
  files are the files with the observational constraints.  

Testing the accuracy of the interpolation
-----------------------------------------

  Requirements:
    * a binary grid of models as produced by SPInS

  Relevant parameters in ``SPInS_configure.py``:
    * ``mode``: set this to ``test_interpolation`` so that SPInS
      will carry out the interpolation tests.
    * ``interpolation_file``: specify the name of the file in which to
      write the results from the interpolation tests in binary format.
      These results can be plotted using :py:mod:`plot_interpolation_test`.

  To run SPInS in this configuration, just type the following command in a
  terminal window::

    ./SPInS.py

  If the grid being tested is two dimensional (excluding age), one can plot
  the results using :py:mod:`plot_interpolation_test` utility::

    ./plot_interpolation_test.py interpolation_test_file

  where ``interpolation_test_file`` is the binary file which contains the
  results from the interpolation test.

Generating an isochrone
-----------------------

  Requirements:
    * a binary grid of models as produced by SPInS

  Relevant parameters in ``SPInS_configure.py``:
    * ``mode``: set this to ``write_isochrone`` so that SPInS
      will generate an isochrone.
    * ``isochrone_parameters``: a dictionary with the isochrone
      parameters.  This must contain all of the grid parameters
      including age, but excluding mass, to generate the isochrone.
    * ``isochrone_npts``: maximum number of points in the isochrone.
      In practise, the number of points will be less because typically
      the target age will lie outside the age range of some of the
      evolutionary tracks which intervene.
    * ``isochrone_file``: name of the file in which to same the
      isochrone in text format.

  To run SPInS in this configuration, just type the following command in a
  terminal window::

    ./SPInS.py

