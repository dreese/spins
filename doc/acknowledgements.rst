Acknowledgements
================

The "Stellar Parameters INferred Systematically" (SPInS) code is a spin-off
of the "Asteroseismic Inference on a Massive Scale" (AIMS) project, one of
the deliverables of the `SpaceINN network <http://www.spaceinn.eu>`_, funded by
the European Community's Seventh Framework Programme (FP7/2007-2013) under grant
agreement no. 312844.  SPInS was initially created for the `5th International
Young Astronomer School <https://gaiaschool.wixsite.com/gaia-school2018>`_ 
"Scientific Exploitation of Gaia Data" held in Paris, (26 February - 2 March
2018), as a simple tool to estimate stellar ages, as well as other stellar
properties.

Publications
------------

If SPInS is used in any publication, the authors kindly ask you to cite the article
`Y. Lebreton & D. R. Reese, 2020, "SPInS, a pipeline for massive stellar parameter
inference", A&A 642, A88 <https://ui.adsabs.harvard.edu/abs/2020A%26A...642A..88L/abstract>`_

