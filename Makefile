F90 = gfortran
FLG = -O2 #-fcheck=all #-fbounds-check
FPY = f2py
#FPY = f2py3 # for python3
#FPY = python3 /full/address/to/f2py # another way of working with python3
#FPY = python3 /usr/local/bin/f2py

spins_fortran: spins_fortran.f90
	$(FPY) -c spins_fortran.f90 -m spins_fortran --fcompiler=$(F90) --f90flags=$(FLG)

clean:
	rm -f spins_fortran.*so

